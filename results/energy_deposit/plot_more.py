import numpy as np
import matplotlib.pyplot as plt

p3gevC_diam=[]
p3gevC_Edep=[]
p3gevC_erEdep=[]
p3gevC_dEdx=[]
p3gevC_erdEdx=[]

p30gevC_diam=[]
p30gevC_Edep=[]
p30gevC_erEdep=[]
p30gevC_dEdx=[]
p30gevC_erdEdx=[]


p04gevW_diam=[]
p04gevW_Edep=[]
p04gevW_erEdep=[]
p04gevW_dEdx=[]
p04gevW_erdEdx=[]


p3gevW_diam=[]
p3gevW_Edep=[]
p3gevW_erEdep=[]
p3gevW_dEdx=[]
p3gevW_erdEdx=[]

# in these files '#' in the first column is a sign of comment
p3gevC_file='Edep_p3GeV_Carbon.dat'
p30gevC_file='Edep_p30GeV_Carbon.dat'
p3gevW_file='Edep_p3GeV_Tungsten.dat'
p04gevW_file='Edep_p400MeV_Tungsten.dat'


C_density=2.0 # [g/cm3]  Carbon
W_density=19.25 # [g/cm3]  Tungsten




with open(p3gevC_file, 'r') as f:
    for line in f:
        if line[0]!="#":
            dane=line.split(',')
            print dane
            p3gevC_diam.append(float(dane[0]))
            p3gevC_Edep.append(float(dane[1]))
            p3gevC_erEdep.append(float(dane[2]))
            p3gevC_dEdx.append(float(dane[1])/(float(dane[0])*C_density/10))
            p3gevC_erdEdx.append(float(dane[2])/(10*float(dane[0])*C_density/10))  # scale error /10 for better visibility


with open(p30gevC_file, 'r') as f:
    for line in f:
        if line[0]!="#":
            dane=line.split(',')
            print dane
            p30gevC_diam.append(float(dane[0]))
            p30gevC_Edep.append(float(dane[1]))
            p30gevC_erEdep.append(float(dane[2]))
            p30gevC_dEdx.append(float(dane[1])/(float(dane[0])*C_density/10))
            print '30 GeV:', p30gevC_diam[-1], p30gevC_dEdx[-1]
            p30gevC_erdEdx.append(float(dane[2])/(10*float(dane[0])*C_density/10))  # scale error /10 for better visibility


with open(p04gevW_file, 'r') as f:
    for line in f:
        if line[0]!="#":
            dane=line.split(',')
            print dane
            p04gevW_diam.append(float(dane[0]))
            p04gevW_Edep.append(float(dane[1]))
            p04gevW_erEdep.append(float(dane[2]))
            p04gevW_dEdx.append(float(dane[1])/(float(dane[0])*W_density/10))
            print '400 MeV:', p04gevW_diam[-1], p04gevW_dEdx[-1]
            p04gevW_erdEdx.append(float(dane[2])/(10*float(dane[0])*W_density/10))  # scale error /10 for better visibility


with open(p3gevW_file, 'r') as f:
    for line in f:
        if line[0]!="#":
            dane=line.split(',')
            print dane
            p3gevW_diam.append(float(dane[0]))
            p3gevW_Edep.append(float(dane[1]))
            p3gevW_erEdep.append(float(dane[2]))
            p3gevW_dEdx.append(float(dane[1])/(float(dane[0])*W_density/10))
            p3gevW_erdEdx.append(float(dane[2])/(10*float(dane[0])*W_density/10))  # scale error /10 for better visibility


        
fig = plt.figure()
ax = fig.add_subplot(111)
# plt.plot(diameter, Edep, '-o')  # why it is not printing
# plt.plot(diameter, dEdx, '-o')  # why it is not printing
plt.errorbar(p3gevC_diam, p3gevC_dEdx, yerr=p3gevC_erdEdx,fmt='-o',label='p 3 GeV on Carbon',color='Blue')
plt.errorbar(p30gevC_diam, p30gevC_dEdx, yerr=p30gevC_erdEdx,fmt='-o',label='p 30 GeV on Carbon',color='Red')
plt.errorbar(p3gevW_diam, p3gevW_dEdx, yerr=p3gevW_erdEdx,fmt='-o',label='p 3 GeV on Tungsten',color='Green')

#print dEdx
ax.set_xlabel('diameter [mm]', color='Blue',fontsize=16)
ax.set_ylabel(r'target dE/dx $\rm [MeV\cdot cm^2/g]$', color='Blue',fontsize=16)
for label in (ax.get_xticklabels() + ax.get_yticklabels()):
    label.set_fontname('Arial')
    label.set_fontsize(14)
# plt.yscale('log')
#plt.text(10, 1.4, "Carbon wire, 2 g/cc", fontsize=14)
#plt.text(10, 1.35, "3 GeV proton beam", fontsize=14)
plt.text(10, 1.30, "Geant 4.10.01", fontsize=14)
ax.grid(True)
plt.xscale('log')
# recompute the ax.dataLim
# axa.relim()
plt.legend(loc='lower right', shadow=False)        



plt.show()       
        
