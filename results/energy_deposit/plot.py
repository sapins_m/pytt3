"""
plotting single plot showing dE/dx versus diameter of the wire
"""
import numpy as np
import matplotlib.pyplot as plt


diameter=[]
Edep=[]
errEdep=[]
dEdx=[]
errdEdx=[]

# adjust filename
# filename='Edep_p3GeV_Carbon.dat'
filename='Edep_p3GeV_Tungsten.dat'

# adjust material density
# density=2.0 # [g/cm3]  Carbon
density=19.25 # [g/cm3]  Tungsten

with open(filename, 'r') as f:
    for line in f:
        if line[0]!="#":
            dane=line.split(',')
            print dane
            diameter.append(float(dane[0]))
            Edep.append(float(dane[1]))
            errEdep.append(float(dane[2]))
            dEdx.append(float(dane[1])/(float(dane[0])*density/10))
            errdEdx.append(float(dane[2])/(10*float(dane[0])*density/10))  # scale error /10 for better visibility
        
fig = plt.figure()
ax = fig.add_subplot(111)
# plt.plot(diameter, Edep, '-o')  # why it is not printing
# plt.plot(diameter, dEdx, '-o')  # why it is not printing
plt.errorbar(diameter, dEdx, yerr=errdEdx,fmt='-o',label='horizontal')

print dEdx
ax.set_xlabel('diameter [mm]', color='Blue',fontsize=16)
ax.set_ylabel(r'target dE/dx $\rm [MeV\cdot cm^2/g]$', color='Blue',fontsize=16)
for label in (ax.get_xticklabels() + ax.get_yticklabels()):
    label.set_fontname('Arial')
    label.set_fontsize(14)
# plt.yscale('log')
plt.text(10, 1.4, "Carbon wire, 2 g/cc", fontsize=14)
plt.text(10, 1.35, "3 GeV proton beam", fontsize=14)
plt.text(10, 1.30, "Geant 4.10.01", fontsize=14)
ax.grid(True)
plt.xscale('log')
# recompute the ax.dataLim
# axa.relim()
plt.show()       
        
