# -*- coding: utf-8 -*-

'''

    This is a main simulation file for the case of the moving SEM grid for J-PARC MCR injection.
    Concerned device names are: MWPM 3,4,5.
    The injected particles are H-. The dE/dx is calculated as a sum of dEdx of a proton and 2 electrons.

    To run: > python sim.py
    2016.09.29 m.sapinski@gsi.de

    remark: for the moment no tilt wrt.the beam is taken into account

'''

from modules import ttbeam, ttarget, ttsim, ttout, ttphys
import math
import numpy
import matplotlib.pyplot as plt

##########################################################
#               define simulation type/title             #
##########################################################
# it is wire scanner type,but in fact it is a slowly moving (a step per beam pulse) SEM grid
ttsim.case = "WireScanner"  # or mixed? MovingGrid
ttsim.title="J-PARC Multi-Wire Profile Monitor"  # use for printout


##########################################################
#                   define the beam                      #
#               J-PARC 400 MeV linac                     #
##########################################################
ttbeam.sigx = 1.3 # [mm]
ttbeam.sigy = 1.5 # [mm]
ttbeam.npart = 0.2*8e13      # 100 beam pulses assumed, pulse frequency: 1 Hz; 8e13 is maximum intensity, task here is
                         # to find which fraction of the maximumintensity can be safely scanned
                         # each pulse lasts 50 us, the rest is time for cooling
ttbeam.trev = 0.2        # [s] repetition period
ttbeam.pulseLength = 50.0e-6     # [s] pulse length
# ttbeam.init() # not needed?


##########################################################
#               define the target/detector               #
#                   100 um tungsten wire                 #
##########################################################
# stopping power of 400 MeV protons in tungsten (beam is H-, but electrons immediately dissociate and their energy can be neglected):
ttarget.dEdx=1.1631e2    # [MeV*mm2/g], from http://physics.nist.gov/cgi-bin/Star/ap_table.pl

# overwrite default target definition:
ttarget.depth = 0.1      # [mm] wire thickness
ttarget.vw = 0.1         # [mm/s] average speed, it is really one stepper second (taken into account by simulation)
ttarget.dh = 0.1         # [mm]

# target is 100 um thick gold-platted tungsten wire
ttarget.material="W"  # tungsten
ttarget.density = 19.25e-3  # [g/mm3]
ttarget.molarMass = 183.84
ttarget.awFun = 5.1                       # [eV], wikipedia says: 5.1 – 5.47 eV
ttarget.wfun = ttarget.awFun * ttphys.Qe  # work function in Joules, as needed
# from http://www.engineeringtoolbox.com/emissivity-coefficients-d_447.html
ttarget.emissivity=0.47   # not polished, maybe this value is too high?

ttarget.init()  # need to run this
print 'number of bins along the wire = ',len(ttarget.xPos)


##########################################################
#       define the rest of the simulation parameters     #
##########################################################
# the beginning and the end of the scan position:
ttsim.startScan = -1.0  # [mm] whole scan is 100 steps so 10 mm
ttsim.endScan = 0.0     # [mm]
#ttsim.dt=1.0    # time step is 1 s, because this is step of the wires movement, but sub-steps are used inside
# above is wrong!!! overestimates the cooling
ttsim.dt=2*ttbeam.pulseLength

# overwrite which cooling processes should be executed:
ttsim.radiativeCooling=True
ttsim.thermionicCooling=True
ttsim.conductiveCooling=False

#########################################################
#   run tests
print "testing, the energy deposit with the first shot:"
print ttbeam.getPartFluxOnWireCell(5., 0.)

#print "plotting the 2D beam transverse profile:"
#ttbeam.plot_beam()

suma=0
print 'test howmany protons on the wire there should be in the first shot:'
for idex, bcen in enumerate(ttarget.xPos):
    suma+=ttbeam.getPartOnWireCell(ttsim.startScan, bcen)

print suma

##########################################################
#           run main simulation                          #
##########################################################
print "running the simulation:"
ttsim.runLinacWireScanner()

ttout.saveTempProfile('JPARC_MWPM_tprof.csv')

ttout.saveMaxTempEvol('JPARC_MWPM_tempEvol.csv')

#########################################################
# analyse/save/plot output
ttout.plotMaxTempEvol('JPARC_MWPM_tprof.png')

