# -*- coding: utf-8 -*-

'''

    This is a simulation file for the case of the J-PARC MR (Main Ring) flying wire.
    To run: > python sim_JPARC_MR_inj.py
    2016.10.03 m.sapinski@gsi.de

'''

from modules import ttbeam, ttarget, ttsim, ttout, ttphys
import math
import numpy
import matplotlib.pyplot as plt


##########################################################
#               define simulation type/title             #
##########################################################
ttsim.case = "WireScanner"
ttsim.title="J-PARC Main Ring Flying Wire"  # use for printout



##########################################################
#                   define the beam                      #
#               J-PARC 3 GeV MR beam at injection        #
##########################################################


ttbeam.sigx = 8.8*0.351
ttbeam.sigy = 8.9*0.351
ttbeam.npart = 3.48e13    # total intensity of the circulating beam

ttbeam.trev = 5e-6        # [s] revolution period


##########################################################
#               define the target/detector               #
#                 7 um carbon wire                       #
##########################################################
# stopping power of 3 GeV protons in carbon:
ttarget.dEdx=1.755e2    # [MeV*mm2/g], from http://physics.nist.gov/cgi-bin/Star/ap_table.pl
# 3 GeV: 1.755e2, 10 GeV: 1.881 [MeV*mm2/g]


# overwrite default target definition:
ttarget.depth = 0.007  # [mm] wire thickness
ttarget.vw = 5000      # [mm/s] one stepper second
ttarget.dh = 0.1         # [mm]

# target is 7 um thick carbon fiber
ttarget.material="C"  # carbon
ttarget.density = 1.7e-3  # [g/mm3]
ttarget.molarMass = 12.01
ttarget.awFun = 4.5                       # [eV], wikipedia says: 5.1 – 5.47 eV
ttarget.wfun = ttarget.awFun * ttphys.Qe  # work function in Joules, as needed
# from http://www.engineeringtoolbox.com/emissivity-coefficients-d_447.html
ttarget.emissivity=0.98   # not polished, maybe this value is too high?

ttarget.init()  # it is obligatory to run this!!! (incorporate in runWireScanner()?)


##########################################################
#       define the rest of the simulation parameters     #
##########################################################
# overwrite the end of the scan position:
ttsim.startScan=-3.0 * ttbeam.sigy
ttsim.endScan=20 * ttbeam.sigy
print "target thickness = ", ttarget.vw
ttsim.dt=ttarget.depth/ttarget.vw

# overwrite which cooling processes should be executed:
ttsim.radiativeCooling=True
ttsim.conductiveCooling=True
ttsim.thermionicCooling=True
ttsim.sublimation=True


#print "testing, the total beam intensity:"
#print ttbeam.getPartOnWireCell(0., 0., 20.0, 20.0)[0]

#print "plotting the 2D beam transverse profile:"
#ttbeam.plot_beam()

print "running the simulation:"
ttsim.runWireScanner()

ttout.plotMaxTempEvol('JPARC_MR_30GeV_tempEvol.png')

ttout.saveTempProfile('JPARC_MR_30GeV_20sigma.csv')

ttout.saveMaxTempEvol('JPARC_MR_30GeV_tempEvol.csv')

