'''

 This module describes particle beam for the need of thin target modelling.

 Input units: mm, g, s, MeV, MeV/u

 2016.08.15 by m.sapinski@gsi.de 
 2023.02.17 mariusz.sapinski@psi.ch
 - convert from module to class


'''

import numpy
import matplotlib.pyplot as plt

from numpy.random import uniform #, seed
from scipy.interpolate import griddata
from matplotlib import cm
from scipy.integrate import dblquad


class Beam:  

    def __init__(self,BeamFileName=None,debug=False):
        '''
        Class constructor. 
        Initializes Parameters.
        Can read a file (to do!).
        Should be able to convert Npart and trevol to paps (to do!)
        ----------
        Returns
        -------
        None.

        '''

        # setup, maybe move to configuration file
        # these are default values, they are normally changed in main simulation script
        # sigx and sigy are gauss sigmas but maybe can have different meaning for non-gaussian beams
        self.sigx=0.5          # [mm] horizontal beam size (in the direction of scan in case of WS)
        self.sigy=0.3          # [mm] vertical beam size
        # particle type, beam energy, charge is it really useful?
        self.ene=590           # [MeV/u]
        self.ptype='proton'    # particle type
        self.pcharge=1         # particle charge [Z]
        self.pmass=938.0         # [MeV/c2]
        self.dedxef=200.0      # [MeV*mm2/g] effective stopping power (electrons and wire shape effect accounted) - move it outside?
        # comment: stoping power is a property of projectile and target, but projectile
        # energy plays usually more important role, so it is placed in ttbeam module.
        # Convinient way to get stopping powers for protons is pstar database:
        # http://physics.nist.gov/cgi-bin/Star/ap_table.pl
        # here it is important again
        self.paps=1.248e15     # average current in particles per second, this default value correspond to 2 mA      
        # paps can be calculated for circular machines from trev, npart etc.
        self.pulse=float('inf')  # [s] pulse length
        
        #trev=89e-6        # [s] beam revolution period (useful for synchrotrons and wire scanners
                  # [s] beam repetition period
        #npart=1.1e11      # number of particles in the bunch/pulse
        #pulseLength=50e-6 # [s] length of the pulse (for pulsed beam) or bunch, if bunch effects considered
        #return 0


    # beam center is by definition in (0,0) 
    # tested, integral gives 1.0
    def gaus2d(self,x,y):
        '''
        Simple function returning the gaussian normalized to full beam intensity (np).
        Unit of the returned value is: [1/mm2]. After inttegration gives numer of particles.
        '''
        a = self.paps/(2*numpy.pi*self.sigx*self.sigy)
        g = a*numpy.exp(-0.5*((x/self.sigx)**2 +(y/self.sigy)**2))
        return g



    # contour plotting function from:
    # http://stackoverflow.com/questions/26999145/matplotlib-making-2d-gaussian-contours-with-transparent-outermost-layer
    # to tools?
    def plot_countour(self,x,y,z):
        # define grid.
        xi = numpy.linspace(-5*self.sigx, 5*self.sigx, 100)  # make arrays depend on beam sigma
        yi = numpy.linspace(-5*self.sigy, 5*self.sigy, 100)  #
        ## grid the data.
        zi = griddata((x, y), z, (xi[None,:], yi[:,None]), method='cubic')
        # contour the gridded data, plotting dots at the randomly spaced data points.
        CS = plt.contour(xi,yi,zi,6,linewidths=0.5,colors='k')
        #CS = plt.contourf(xi,yi,zi,15,cmap=plt.cm.jet)
        CS = plt.contourf(xi,yi,zi,6,cmap=cm.Greys_r)
        #plt.colorbar() # draw colorbar
        # plot data points.
        plt.xlim(-5*self.sigx,5*self.sigx)
        plt.ylim(-5*self.sigy,5*self.sigy)
        # plt.title('griddata test (%d points)' % npts)
        plt.show()


    def plot(self):
        # make up some randomly distributed data
        #seed(1234)
        npts = 2000  # arbitrary No of points
        print(( 'plotting transverse profile of the beam with sigmax = ',self.sigx))
        x = numpy.random.uniform(-5*self.sigx,5*self.sigy,npts)
        #x = numpy.random.uniform(ttsim.startScan,ttsim.endScan,npts)
        y = numpy.random.uniform(-5*self.sigx,5*self.sigy,npts)
        #z = gauss2d(x,y,Sigma=np.asarray([[1.,.5],[0.5,1.]]),mu=np.asarray([0.,0.]))
        z=list(map(self.gaus2d,x,y))
        self.plot_countour(x,y,z)
    
    
    def part_cell(self,ay,ax,d=None,dh=None):
        '''
        Returns number of beam particles per second in a cell of a wire.
        Works for case of SEM grid and beam pulse.
        Arguments are:
            y-[mm] distance of the wire from the beam center 
            x-[mm] distance of the wire cell from the wire center
            d-[mm] wire diamterer
            dh-[mm] length of the wire cell
        '''
        if d==None:
            from . import ttarget
            d=ttarget.depth
        if dh==None:
            from . import ttarget
            dh=ttarget.dh
        npts=dblquad(self.gaus2d,ay-d/2,ay+d/2, lambda x: ax-dh/2, lambda x: ax+dh/2, epsrel = 1e-9, epsabs = 0)
        return npts[0]



