'''
    ttphys

    The physics module: contain physics constants
    2016.08.24: m.sapinski@gsi.de

'''

# -*- coding: utf-8 -*-

import math
import numpy
import matplotlib.pyplot as plt
from modules import ttarget
from scipy import constants



# Stefan or Stefan-Boltzman constant for blak-body radiation law
pSB=5.67e-12              #  [J/(s cm2 K4)] Stefan-Boltzman constans
STEFAN=pSB*1.0/100        #  [J/(s mm2 K4)] corrected to simulation units


# Richardson constant; not in scipy.constants, because it is not fundamental:
# https://en.wikipedia.org/wiki/Thermionic_emission
Richardson=1.20173           # [A/(mm^2/K^2)] Richardson constant

# elementary electric charge
Qe=1.60218e-19            # [C] - elementary charge


# include Bethe-Bloch from beammatter module


def thermionic_current(surf,temp,wfun):
    '''
    Computes thermionic current in [A]
    Parameters
    ----------
    surf : float
        surface [mm^2]
    temp : float
        temperature [K]
    wfun : float
        work function [eV]

    Returns
    -------
    float
        thermionic current [A]
    '''
    return surf*Richardson*pow(temp,2)*numpy.exp(-1*wfun/(constants.Boltzmann*temp))


# function to calculate thermionic current spectrum!!!


'''
# for valpur pressure:
Double_t
fBoltzmann = 1.38056e-16; // [cm2 * g / (s2 * K)]
Double_t
fCmolarMass = 12.01; // [g / mole]
Double_t
fCatomMass = fCmolarMass * 1.661e-24; // [g]
mass
of
one
carbon
atom
Double_t
fConst1 = pow(2 * fBoltzmann / (fCatomMass * TMath::Pi()), 0.5); // << to
use
later
Double_t
V0 = 22.4e3; // [cm3 / mole]
Double_t
P0 = 1.01e5; // [Pa]
atmospheric
pressure(1
atmosphere)
Double_t
T0 = 293.; // [K] - standard
temp(0
degree
of
Celsius)
Double_t fConst2 = fCmolarMass * T0 / (P0 * V0); // [g * K / Pa * cm3]

// sublimation (in the center) according to Dushman book
  Double_t Subcoeff_B=40.03e+3;
  Double_t sub_logW=0;
  Double_t sub_W=0;
  Double_t sub_Wdt=0;

'''