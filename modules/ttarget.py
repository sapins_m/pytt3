'''

 This module describes target class which carries info about target geometry (not material)
 and state (ie. temperature distribution, stress, electron emission currents)

 2016.08.15 by m.sapinski@gsi.de 
 2016.10.06: move sublimation material constant here from ttphys.
 2023.02.17: mariusz.sapinski@psi.ch
   - converted to class
     
'''

import numpy, math
from modules import ttbeam, ttphys, ttmaterial
from scipy.interpolate import interp1d
from modules import ttmaterial
from pathlib import Path



class Target:
    
    def __init__(self,material_file,debug=False):
        self.case = "Wire" # other options: 'Foil'
        # common for all cases
        # dw  = 0.03             # [mm] - target thickness, in case of wire-diameter
        # rho = 2e-3             # [g/mm3] - target density
        # wFun= 4.5              # [eV] work function for graphite   2do: temperature-depndent work function!!!
        # wfun=wFun*ttphys.Qe    # [J] ---||----

        print("Target init:")
        self.Material = ttmaterial.Material(material_file)
        self.surfMaterial='C'         # sometimes surface material is different than bulk material - not used now

        # geometry
        self.depth = 0.03             # [mm] - target thickness, in case of wire-diameter
        self.cell_length = 2.e-2      # [mm] replaces dh, cell size (vertical) - it is updated in ttwirescan constructor
        self.wire_length = 10         # [mm], - it is updated in ttwirescan constructor

        # scan speed
        self.speed = 30               # [mm/s] replaces vw, 0 if not scanner

        # 1D case, moving of the wire:
        #vw = 1000  # [mm/s] wire speed
        #dh = 2.e-3  # [mm] bin size along the wire


        # vapHeat = 715000  # [J / mol] vaporization heat for Carbon => ttmaterial

        ################################################################################################
        # lists describing the target wire slicing and initial conditions (surfaces and volumes):
        # these initialisations are good for wires (for now)
        # this should be recalculated, right?
        #xPos = numpy.arange(-4.0 * ttbeam.sigx, 4.0 * ttbeam.sigx - dh, dh)  # list of bin centers
                                            # (other proposed names: exPos, ePosX, cPosX, for cell)
        self.xPos=list() # steps along the scan, empty for SEM grid or Screen
        # do we need yPos
        self.cell_pos = list() # list of cell positions along the wire
        #eRad = [depth / 2 for i in range(len(xPos))]  # initial radiuses of wire elements (or mean size of pie-shaped screnne elements)
        self.depths=list() # wire depths (diamteters) along the wire; could be loaded from file!
        #eVol = [math.pi * depth ** 2 * dh / 4 for i in range(len(xPos))]  # initial volumes of target elements,
        #eVol=list() # do we need this here? can be calculated dynamically!

        #eSurf = [math.pi * depth * dh for i in  range(len(xPos))] # initial values of the list of elements' external surfaces (radiating surface)
        #eSurf=list()  # calculate dynamically
        #eCSurf = [math.pi * depth **2/4 for i in  range(len(xPos))]# list of cross-section surfaces (conductive surfaces)
        #eCSurf=list()

        #############################
        # lists describing target wire slices condition in each simulation step:
        self.paac = list()  # list of numers of particles accumulated in each target/wire cell
        self.temperature = list()  # list of temperatures in cells/fragments and for given time steps (2D)
        self.thcurr=list()  # list of thermionic currents from cells/fragments and for give time steps (2D)
        self.securr=list()  # secondary emission current from cells (2D)
        # there should be also list with stress values which depend on pre-stress and thermal expansion
        # used to calculate when mechanical damage takes place


    def default_init(self,sigy):
        '''
        default initialization
        :return:
        nothing
        '''
        print("default initialization of ttarget")
        self.xPos = list()  # list of wire positions, 
        self.ncells= int(6*sigy/self.cell_length) # number of cells along the wire
        self.depths = [self.depth for i in range(self.ncells)]  # initial radiuses of wire elements (or mean size of pie-shaped screnne elements)
        self.temperature=[300.0 for i in range(self.ncells)]     # room temperature state at the beginning
        self.thcurr = [ 0.0 for i in range(self.ncells) ]  # room temperature state at the beginning








