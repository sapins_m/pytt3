'''
    ttsim

    The simulation module (now only for the case of the flying wire).
    Contains beam heating and cooling procedures... maybe to be distributed to
    various submodules, eg. tthermionic.
    2016.08.16 m.sapinski@gsi.de

'''

# -*- coding: utf-8 -*-

from modules import ttphys, ttbeam, ttarget, ttout, ttools
import math
import numpy
import matplotlib.pyplot as plt
from scipy.interpolate import interp1d
from scipy import constants


# =====================================
# important parameters
# dt=0.001  # [s] - time step
dt = ttarget.depth / ttarget.vw  # this is maximum simulation step, better decrease it (check temperature between steps)
# dt *= 0.5 # decreasing the step
# print 'time step is ', 1e6*dt, ' [us]'

tempAmb = 300  # [K] ambient temperature (to ttarget?)

title="Simulation Title"
wirePositions = []  # output
nparts = []  # auxiliary anParts?
verbose = 1  # verbose level

movementTimes = []  # list of timestamps for consecutive simulation steps
maximumTemps = []   # maximum temperature evolution list
npartProf= []       # actual number of particles crossing the beam in a given moment
                    # (for the purpose of beam shape plotting)
SEMcurr=[]          # secondary emission current 

# default values, can be changed from the main script:
radiativeCooling=True
conductiveCooling=False
thermionicCooling=True
thermCurrentHeating=False
sublimation=True


# scan start and end:
startScan=-4.0 * ttbeam.sigy  # initial position of the wire
endScan = 6 * ttbeam.sigy     # final position of the wire


#######################################################
#   synchrotron WireScanner case
#   loop over the wire movement
#######################################################
def runWireScanner():
    '''
    Function which runs the Wire Scanner simulation
    '''
    print("run WireScanner starts")
    # scan start and end:
    startScan=-4.0 * ttbeam.sigy  # initial position of the wire
    endScan = 6 * ttbeam.sigy     # final position of the wire
    posy = startScan             # initial position
    tAcc = 0.0                   # simulation start time
    print(startScan,posy,endScan)          # why they are equal?
    ###################################################
    #   main loop over scan movement
    ###################################################
    while posy < endScan:
        print("posy = ",posy)
        # loop along the wire:
        posx = -4.0 * ttbeam.sigx  # initial position of the wire   # remove? only posy important,posx replaced by iterators
        tAcc = tAcc + dt  # increment accumulated time  needed? rm?
        # loop over wire elements to create temporary array of number of particles crossing the wire slice in
        # the given step (nparts is cleaned at the end off the step):
        for idex, bcen in enumerate(ttarget.xPos):
            # print bcen, idex
            nparts.append(dt * ttbeam.getPartFluxOnWireCell(posy, bcen, 2*ttarget.eRad[idex], ttarget.dh))

        # summing all particles crossing the wire, for later cross-check and plotting of the measured profile:
        npartProf.append(sum(nparts))

        # adding charge emmited due to secondary emission
        SEMcurr.append(constants.elementary_charge*1e6*sum(nparts)*SEY()/dt)

        ###################################################################
        # adding particles to accumulative lists ttarget.eAccNparts
        if len(ttarget.eAccNparts) == 0:  # initialization step
            ttarget.eAccNparts = nparts[:]   # just copy the list in the first step
            ttarget.eTemp = [300.0 for i in range(len(ttarget.eAccNparts))]       # room temperature at the beginning
    #        line0, = ax1s[0].plot(ttarget.xPos, ttarget.eTemp, color='Blue')   # initial plot, moved to simPlotIni
    #        line1, = ax1s[1].plot(ttarget.xPos, ttarget.eRad, color='Green')      # initial plot
        else:
            ttarget.eAccNparts = [x + y for x, y in zip(ttarget.eAccNparts,
                                                        nparts)]  # adding number of particles which go through the wire

        ######################################################
        # heat the wire with the beam
        runBeamHeating()

        # debug:
        # print 'heating - time:', tAcc*1e6, ' [us],  wire pos: ', posy, ' [mm],  max temp: ', max(ttarget.eTemp), ' [K]'


        # run coolings.
        # is order important? should it be "apply coolings" first and then run it alltogether
        if radiativeCooling:
            dtemp_radiative=runRadiativeCooling(option='delay')
        # debug:
        #print 'radCool - time:', tAcc*1e6, ' [us],  wire pos: ', posy, ' [mm],  max temp: ', max(ttarget.eTemp), ' [K]'
        if thermionicCooling:
            dtemp_thermionic=runThermionicCooling(option='delay')
        if conductiveCooling:
            dtemp_conductive=runConductiveCoolingWire2(option='delay')
        else:
            dtemp_conductive = [0 for i in range(len(ttarget.eTemp))]
        # debug:
        #print 'eTemp = ', ttarget.eTemp, len(ttarget.eTemp)
        #print 'radiative = ',dtemp_radiative, len(dtemp_radiative)
        #print 'thermionic = ',dtemp_thermionic, len(dtemp_thermionic)
        #print 'conductive = ',dtemp_conductive, len(dtemp_conductive)

        if sublimation:
            runSublimation()
        # to do:
        #if thermCurrentHeating:
        #    runThermCurrentHeating()

        # subtracting all cooling effects together
        for idex, bcen in enumerate(ttarget.xPos):
            ttarget.eTemp[ idex ] -= (dtemp_radiative[idex]+dtemp_thermionic[idex]+dtemp_conductive[idex])

        ttout.eTempProfEvol1D[tAcc] = list(ttarget.eTemp)  # list needed for deep copy, update ttout

        wirePositions.append(posy)  # not important,rm?
        movementTimes.append(tAcc)
        maximumTemps.append(max(ttarget.eTemp))

        ttout.simPlotUpdate(movementTimes, maximumTemps, npartProf)

        print(( 'time:', tAcc*1e6, ' [us],  wire pos: ', posy, ' [mm],  max temp: ', max(ttarget.eTemp), ' [K]'))
        posy += ttarget.vw * dt
        del nparts[:]  # important line!!! otherwise nparts becomes veery long and only first part is used
    # ax.clf() # clearing the figure at the end... not needed anymore



#######################################################
#   linac Wire Scanner case
#   loop over beam pulses and wire steps
#######################################################
def runLinacWireScanner():
    ###################################################
    #   main loop over scan movement
    ###################################################
    posy=startScan
    tAcc=0
    del nparts[ : ]
    thcurr=[]
    while posy < endScan:
        # prototyping
        dposy=ttarget.depth  # provisional
        time_in_pulse=0
        for idex, bcen in enumerate(ttarget.xPos):
            # print bcen, idex
            # in case of single pulse:
            nparts.append(ttbeam.getPartOnWireCell(posy, bcen))
        print(( posy,sum(nparts),nparts))
        # shot!
        runBeamHeating()

        ttout.eTempProfEvol1D[tAcc] = list(ttarget.eTemp)  # list needed for deep copy, update ttout

        #tAcc+=ttbeam.pulseLength  # immediately do some cooling
        print( 'shot!')
        # run cooling here too?
        # run only cooling to the end of the repetition period
        while time_in_pulse<ttbeam.trev:
            if thermionicCooling:
                thcurr=runThermionicCooling(option='instant', delta_t=10*dt)
            if radiativeCooling:
                runRadiativeCooling(option='instant', delta_t=10*dt)
            if conductiveCooling:
                runConductiveCoolingWire3(option='instant')   # implement delta_t too
            if sublimation:
                runSublimation()
            # optimizing simulation step
            time_in_pulse+=10*dt    # short for high temperatures
            #if max(ttarget.eTemp)<400:
            #    time_in_pulse += 20 * ttbeam.pulseLength  # long for lower temperatures
            movementTimes.append(tAcc)
            maximumTemps.append(max(ttarget.eTemp))
            # summing all particles crossing the wire, for later cross-check and plotting of the measured profile:
            npartProf.append(sum(nparts))

            ttout.eTempProfEvol1D[ tAcc ] = list(ttarget.eTemp)  # list needed for deep copy, update ttout
            ttout.eThCurrProfEvol1D[ tAcc ] = list(thcurr)  # list needed for deep copy, update ttout
            del thcurr[:]
            print(( 'updating plot...', time_in_pulse, tAcc))
            ttout.simPlotUpdate(movementTimes, maximumTemps, npartProf)

            tAcc+=10*dt
            #if max(ttarget.eTemp)<400:
            #    tAcc += 20 * ttbeam.pulseLength
                # modify ttsim.dt too
        #move:
        posy+=dposy
        time_in_pulse=0
        #movementTimes.append(tAcc)
        #maximumTemps.append(max(ttarget.eTemp))
        #ttout.simPlotUpdate(movementTimes, maximumTemps, npartProf)
        del nparts[ : ]  # important line!!! otherwise nparts becomes veery long and only first part is used


def runBeamHeating():
    '''
    Beam heating is calculated here. It is "instantenous,", no dt is needed.
    '''
    for idex, bcen in enumerate(ttarget.xPos):
        a_cp = ttarget.getSHeat(ttarget.material,ttarget.eTemp[idex])
        #print("ttsim::runBeamHeating a_cp=",a_cp)
        # coeffcient pi/4 is for round wire case (ration of circle surface to square surface)
        # This formula is OK. Material density cancels out in nominator and denominator.
        dtemp = math.pi * ttbeam.dEdx * 1.60218e-13 * 2 * ttarget.eRad[idex]
        dtemp = dtemp * nparts[idex] 
        dtemp = dtemp / (4 * a_cp * ttarget.eVol[idex])
        #print 'heating: ', idex, bcen, ttarget.eTemp[idex], dtemp
        # print dtemp, nparts[idex]
        ttarget.eTemp[idex] += dtemp


def runRadiativeCooling(option='delay', delta_t=dt):
    # type: (object) -> object
    '''
    Here radiative cooling is calculated.
    delta_t parameter is considered duration of the cooling process. Normally it is defined by dt, but in order to accelerate
    simulation can be increased, when conditions do not change too fast.
    '''
    dtemp_list=list()
    for idex, bcen in enumerate(ttarget.xPos):
        cp = ttarget.getSHeat(ttarget.material,ttarget.eTemp[idex])
        # energy evacuated by radiative cooling in dt:
        dene = ttarget.eSurf[idex] * ttphys.STEFAN * ttarget.emissivity * (
            pow(ttarget.eTemp[idex], 4) - pow(tempAmb, 4)) * delta_t
        dtemp = dene / (cp * ttarget.eVol[idex] * ttarget.density)
        dtemp_list.append(dtemp)
        if option=='instant':
            ttarget.eTemp[idex] -= dtemp
    if option=='delay':
        return dtemp_list
    else:
        return 'None'


def runThermionicCooling(option='delay', delta_t=dt):
    '''
    Here thermionic cooling is calculated.
    Beware: traditional Richardson's law derived for bulk materials is invalid for graphene,
            (doi:10.1103/PhysRevApplied.3.014002)
    returns: list of temperature differences
        or: list of thermionic currents... >>> this is limiting!
    '''
    dtemp_list=list()
    thcurr_list=list()
    for idex, bcen in enumerate(ttarget.xPos):
        cp = ttarget.getSHeat(ttarget.material,ttarget.eTemp[idex])
        # thermionic current [A] (total, not density):
        # (2do: array out of that (thermionic current also of interest))
        thCurrent = ttarget.eSurf[idex] * ttphys.RICHARDSON * ttarget.eTemp[idex] ** 2 * numpy.exp(
            -1 * ttarget.wfun / (ttphys.BOLTZMANN * ttarget.eTemp[idex]))
        thcurr_list.append(thCurrent)
        # energy evacuated by thermionic cooling in time dt:
        # (remark1: each electron removes energy equal to work funtion plus its thermal energy)
        # (remark2: for explanation of 2kT see M. Sapinski, Thermionic versus radiative cooling , EDMS 1056423
        # basically 1kT is for electrons emitted perpendicular to the surface and the other 2*1/2kT is for
        # two degrees of freedom parallel to the surface
        # (remark3: units: wfun block [J], tcur [A], 
        dene = (ttarget.wfun + (2 * ttphys.BOLTZMANN * ttarget.eTemp[idex])) * thCurrent * delta_t / ttphys.Qe
        dtemp = dene / (cp * ttarget.eVol[idex] * ttarget.density)
        dtemp_list.append(dtemp)
        if option=='instant':
            ttarget.eTemp[idex] -= dtemp
    if option=='delay':
        return dtemp_list
    elif option=='instant':
        return thcurr_list
    else:
        return 'None'


def runConductiveCoolingWire(option='delay'):
    '''
    First version, conservation of energy not imposed. Fourier law (linear heat transfer) used.
    2016.08.26 - some error, not working (unstable)
    :return:
    '''
    print( 'Conductive Cooling using fourier equation ---' ) # temporary
    dtemp_list=[]  # list of temperature changes
    for idex, bcen in enumerate(ttarget.xPos):
        #print 'x'  # temporary
        cp = ttarget.getSHeat(ttarget.material,ttarget.eTemp[idex])
        la = ttarget.getThConductivity(ttarget.material,ttarget.eTemp[idex])
        # fix temperature at the wire extremities:
        #ttarget.eTemp[0]=300.0
        #ttarget.eTemp[len(ttarget.xPos)-1]=300.0
        # these two lines below allow to stabilise the solution for conductive cooling,
        # they assume that the carbon wire is attached to a material with a very good thermal conductivity!
        # future: simulate heat flow through the fork?
        #leftForkTemp=ttarget.eTemp[0]   # [K]
        #rightForkTemp=ttarget.eTemp[len(ttarget.xPos)-1]  # [K]
        leftForkTemp=math.pow(ttarget.eTemp[0],2)/ttarget.eTemp[1]   # [K]
        if leftForkTemp<300:
            leftForkTemp=300
        rightForkTemp=math.pow(ttarget.eTemp[-1],2)/ttarget.eTemp[-2]  # [K]
        if rightForkTemp<300:
            rightForkTemp=300
        # convention:
        # positive dene - energy leaving the given wire element
        # negative
        dene_left=0
        dene_right=0
        if idex==0:
            dene_left= -la * ttarget.eCSurf[idex] * (ttarget.eTemp[idex] - leftForkTemp) * dt / ttarget.dh
            dene_right = -la * ttarget.eCSurf[idex] * (ttarget.eTemp[idex] - ttarget.eTemp[idex+1]) * dt / ttarget.dh
        if idex>0 and idex < len(ttarget.xPos)-1:
            dene_left= -la * ttarget.eCSurf[idex] * (ttarget.eTemp[idex] - ttarget.eTemp[idex-1]) * dt / ttarget.dh
            dene_right = -la * ttarget.eCSurf[idex] * (ttarget.eTemp[idex] - ttarget.eTemp[idex+1]) * dt / ttarget.dh
        if idex==(len(ttarget.xPos)-1):
            dene_left= -la * ttarget.eCSurf[idex] * (ttarget.eTemp[idex] - ttarget.eTemp[idex-1]) * dt / ttarget.dh
            dene_right = -la * ttarget.eCSurf[idex] * (ttarget.eTemp[idex] - rightForkTemp) * dt / ttarget.dh
        # debugging
        #if idex==len(ttarget.xPos)-1:
            #print "last: ",dene_left, dene_right
        dtemp=(dene_left+dene_right)/(cp * ttarget.eVol[idex] * ttarget.density)
        if dtemp<1.0:
            dtemp=0.0
        dtemp_list.append((dtemp))
        # debugging
        #if idex<4 or idex>len(ttarget.xPos)-10:
        #    print idex,dene_left, dene_right,' Conductive cooling dtemp=',dtemp_list[idex], '  temp=',ttarget.eTemp[idex]
        #    print 'la = ', la, ttarget.eCSurf[idex], ttarget.eTemp[idex]
        # special printout for debugging:
        #if idex==1997:
            # print 'la = ', la, " Csurf = ", ttarget.eCSurf[idex], ttarget.eTemp[idex], ttarget.eTemp[idex-1], ttarget.eTemp[idex+1], ttarget.dh, dt
        if idex>100:
            print(('idex = ',idex,' la = ', la, ' Csurf = ', ttarget.eCSurf[idex], 'dtemp_cond = ',dtemp_list[idex],ttarget.eTemp[idex], ttarget.dh, dt))
            print(( dene_left, dene_right))

            # len(dtemp_list) - OK
    #print ttarget.eTemp
    # will it oerwrite eTemp:
    #ttarget.eTemp=[i - j for i, j in zip(ttarget.eTemp, dtemp_list)]
    # less elegant alternative...
    # why is this not working?
    if option=='instant':
        for idex, value in enumerate(ttarget.eTemp):
            ttarget.eTemp[idex]-=dtemp_list[idex]
        del dtemp_list[ : ]  # is this needed?
    elif option=='delay':
        return dtemp_list


def runConductiveCoolingWire2(option='delay'):
    '''
    Second version, based on heat equation, not fourier equation.
    2016.09.30
    :return:
    '''
    print( 'Conductive Cooling based on heat equation ---')  # temporary
    dtemp_list=[]  # list of temperature changes
    for idex, bcen in enumerate(ttarget.xPos):
        #print 'x'  # temporary
        cp = ttarget.getSHeat(ttarget.material,ttarget.eTemp[idex])
        la = ttarget.getThConductivity(ttarget.material,ttarget.eTemp[idex])
        # fix temperature at the wire extremities:
        #ttarget.eTemp[0]=300.0
        #ttarget.eTemp[len(ttarget.xPos)-1]=300.0
        # these two lines below allow to stabilise the solution for conductive cooling,
        # they assume that the carbon wire is attached to a material with a very good thermal conductivity!
        # future: simulate heat flow through the fork?
        #leftForkTemp=ttarget.eTemp[0]   # [K]
        #rightForkTemp=ttarget.eTemp[len(ttarget.xPos)-1]  # [K]
        #leftForkTemp=300.0   # [K]
        #rightForkTemp=300.0  # [K]
        leftForkTemp=math.pow(ttarget.eTemp[0],2)/ttarget.eTemp[1]   # [K]
        if leftForkTemp<300:
            leftForkTemp=300
        rightForkTemp=math.pow(ttarget.eTemp[-1],2)/ttarget.eTemp[-2]  # [K]
        if rightForkTemp<300:
            rightForkTemp=300
        # convention:
        # positive dene - energy leaving the given wire element
        # negative
        ddTdx2=0  # second derivative of temperature along the wire (1D Laplacian)
        # convention: zero is at the beginning of the wire, and axis direction is towards higher position value
        # so \Delta T = T2-T1
        if idex==0:
            grad1=((ttarget.eTemp[ idex + 1 ] - ttarget.eTemp[ idex ]) / ttarget.dh)  # right gradient
            #grad0=((ttarget.eTemp[ idex ] - leftForkTemp) / ttarget.dh)               # left gradient
            grad0=0.99*grad1  # perfect cooling
            ddTdx2 = grad1 - grad0
            ddTdx2 /= ttarget.dh
            print( 'gradients:')
            print((grad1, grad0))
        elif idex>0 and idex < len(ttarget.xPos)-1:
            ddTdx2 = ((ttarget.eTemp[idex + 1] - ttarget.eTemp[idex]) / ttarget.dh) - ((ttarget.eTemp[idex] - ttarget.eTemp[idex-1]) / ttarget.dh)
            ddTdx2 /= ttarget.dh
            if idex<5:
                print( 'gradients: ')
                print(( ((ttarget.eTemp[idex + 1] - ttarget.eTemp[idex]) / ttarget.dh), ((ttarget.eTemp[idex] - ttarget.eTemp[idex-1]) / ttarget.dh)))
        elif idex==(len(ttarget.xPos)-1):
            grad1=((rightForkTemp - ttarget.eTemp[idex]) / ttarget.dh)
            grad0=((ttarget.eTemp[idex] - ttarget.eTemp[idex-1]) / ttarget.dh)
            grad1=0.99*grad0
            ddTdx2 = grad1 - grad0
            ddTdx2 /= ttarget.dh

        kappa=la/(ttarget.density*cp)
        # strange, no cross-section!
        dtemp=ddTdx2*dt*kappa
        if dtemp<1.0:
            dtemp=0.0
        dtemp_list.append((dtemp))
        # debugging
        if idex<4 or idex>len(ttarget.xPos)-4:
            print((idex,ddTdx2,' Conductive cooling dtemp=',dtemp_list[idex], '  temp=',ttarget.eTemp[idex]))
        #    print 'la = ', la, ttarget.eCSurf[idex], ttarget.eTemp[idex]
        # special printout for debugging:
    #print ttarget.eTemp
    # will it oerwrite eTemp:
    #ttarget.eTemp=[i - j for i, j in zip(ttarget.eTemp, dtemp_list)]
    # less elegant alternative...
    # why is this not working?:
    if option=='instant':
        for idex, value in enumerate(ttarget.eTemp):
            ttarget.eTemp[idex]-=dtemp_list[idex]
    elif option=='delay':
        return dtemp_list
        #if ttarget.eTemp[idex] < 300.0:
        #    ttarget.eTemp[idex]=300.0
        # print 'after: idex = ', idex, ' la = ', la, ' Csurf = ', ttarget.eCSurf[idex], ttarget.eTemp[idex], ttarget.dh, dt
    #del dtemp_list[:] # rm?



def runConductiveCoolingWire3(option='delay'):
    '''
    Third version, based on heat equation, solved using FTCS (forward in time, centred in space) algorithm
    2016.10.03
    :return:
    '''
    # print 'Conductive Cooling based on heat equation solved using FTCS ---'  # temporary
    dtemp_list=[]  # list of temperature changes
    for idex, bcen in enumerate(ttarget.xPos):
        #print 'x'  # temporary
        cp = ttarget.getSHeat(ttarget.material,ttarget.eTemp[idex])
        la = ttarget.getThConductivity(ttarget.material,ttarget.eTemp[idex])
        # linear extrapolation of fork temperatures
        leftForkTemp=math.pow(ttarget.eTemp[0],2)/ttarget.eTemp[1]   # [K]
        if leftForkTemp<300:
            leftForkTemp=300
        rightForkTemp=math.pow(ttarget.eTemp[-1],2)/ttarget.eTemp[-2]  # [K]
        if rightForkTemp<300:
            rightForkTemp=300

        # convention: zero is at the beginning of the wire, and axis direction is towards higher position value
        if idex==0:
            ftcs=ttarget.eTemp[idex+1]-2*ttarget.eTemp[idex]+leftForkTemp
        elif idex>0 and idex < len(ttarget.xPos)-1:
            ftcs=ttarget.eTemp[idex+1]-2*ttarget.eTemp[idex]+ttarget.eTemp[idex-1]
        elif idex==(len(ttarget.xPos)-1):
            ftcs=rightForkTemp-2*ttarget.eTemp[idex]+ttarget.eTemp[idex-1]


        kappa=la/(ttarget.density*cp)
        # strange, no cross-section!
        dtemp=ftcs*dt*kappa/math.pow(ttarget.dh,2)
        # the line below must be done,otherwise problems related to numerical precision when subtracting two small numbers
        # critical! can be used to stabilize runConductiveCoolingWire() and runConductiveCoolingWire2()
        if dtemp<1.0:
            dtemp=0.0
        dtemp_list.append((dtemp))
        # debugging
        #if idex<4 or idex>len(ttarget.xPos)-4:
        #    print idex,' Conductive cooling dtemp=',dtemp_list[idex], '  temp=',ttarget.eTemp[idex]
        #    print 'la = ', la, ttarget.eCSurf[idex], ttarget.eTemp[idex]
        # special printout for debugging:
        #if idex==1997:
            # print 'la = ', la, " Csurf = ", ttarget.eCSurf[idex], ttarget.eTemp[idex], ttarget.eTemp[idex-1], ttarget.eTemp[idex+1], ttarget.dh, dt
        #if idex<1000:
        #    print 'idex = ',idex,' la = ', la, ' Csurf = ', ttarget.eCSurf[idex], 'dtemp_cond = ',dtemp_list[idex],ttarget.eTemp[idex], ttarget.dh, dt

            # len(dtemp_list) - OK
    #print ttarget.eTemp
    # will it oerwrite eTemp:
    #ttarget.eTemp=[i - j for i, j in zip(ttarget.eTemp, dtemp_list)]
    # less elegant alternative...
    # why is this not working?:
    if option=='instant':
        for idex, value in enumerate(ttarget.eTemp):
            ttarget.eTemp[idex]-=dtemp_list[idex]
    elif option=='delay':
        return dtemp_list
        #if ttarget.eTemp[idex] < 300.0:
        #    ttarget.eTemp[idex]=300.0
        # print 'after: idex = ', idex, ' la = ', la, ' Csurf = ', ttarget.eCSurf[idex], ttarget.eTemp[idex], ttarget.dh, dt
    #del dtemp_list[:]



def runSublimation():
    '''
    based on Equations and tables :
    S. Dushman, "Scientific foundations of vacuum technique", Wiley Inc., New York 1962 (second edition).
    Equation: 10.9 and data from Table. 10.2 (see ttphys)
    (see file documents/dipac2009_tupd40_poster.pdf)
    (BTW \bar(5).7660 in Equation 10.10b means -5+0.7660,
    see explanation:
    http://math.stackexchange.com/questions/1157173/understand-logarithm-of-bar-values-manipulation-step)
    :return:
    '''
    # loop along the wire:
    for idex, bcen in enumerate(ttarget.xPos):
        # print 'sublimation, ',idex,ttarget.eTemp[idex]
        sub_logW = ttarget.Sublimation_C2-(0.5*math.log10(ttarget.eTemp[idex]))-(ttarget.Sublimation_C1/ttarget.eTemp[idex])
        sub_W = math.pow(10.0, sub_logW)   # [g / cm2 / s]   material sublimation rate
        sub_Wdt = sub_W * dt/100.0         # [g / cm2 / s] -> [g / mm2]
        dSublimated = sub_Wdt/ttarget.density # thickness of material sublimated in this step
        ttarget.eRad[idex]-=dSublimated
        ##################################################
        # recalculate volumes and surfaces:
        ttarget.eVol[idex]=math.pi*ttarget.eRad[idex]**2 * ttarget.dh
        ttarget.eSurf[idex]=math.pi * ttarget.eRad[idex] * ttarget.dh
        ttarget.eCSurf[idex]=math.pi * ttarget.eRad[idex] **2
        ##################################################
        # sublimation cooling:
        dsublimated_mass = dSublimated*math.pi*ttarget.dh*(dSublimated+2*ttarget.eRad[idex])*ttarget.density
        dsublimated_mol = dsublimated_mass / ttarget.molarMass
        sublim_heat = ttarget.vapHeat * dsublimated_mol # [Joules]
        cp = ttarget.getSHeat(ttarget.material,ttarget.eTemp[idex])
        dtemp=sublim_heat/(cp * ttarget.eVol[idex] * ttarget.density)
        ttarget.eTemp[idex] -= dtemp


def runSublimation2():
    '''
        to do: another sublimation code based on vapour pressure
    :return:
    '''
    # vtemp=[]   # list of temperatures of vapour pressure data
    # vpress=[]  # list of vapour pressures at given temperatures
    # vtemp, vpress = ttools.readVapourPressureData("data/vappress.data")  # not needed? remove?
    # f = interp1d(vtemp, vpress, fill_value='extrapolate')   # finish this
    return 0


def SEY():
    '''
    Computers secondary emission current from Sternglass model.

    Returns
    -------
    value of SE current (sum over wire)

    '''
    # Araceli's thesis, Eq. 2.12
    Ls=1.0/(3.68e-17*ttarget.atomdensity*ttarget.Znr**(1/3.))
    # dEdx in the Equation 2.11 is LET expressed in eV/cm, therefore factor 1e7
    SEYp = 0.01*Ls*ttarget.dEdx*ttarget.density*1e7  # secondary emission yield due to protons
    return SEYp