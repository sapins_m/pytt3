# -*- coding: utf-8 -*-
"""
Created on Tue Feb 21 09:41:52 2023

@author: sapins_m

Class WireScan, controls running Wire Scanner simulation


"""

import numpy, math
from modules import ttbeam, ttphys, ttmaterial, ttarget
from scipy.interpolate import interp1d
from pathlib import Path
from scipy import constants



class WireScan:
    
    def __init__(self,debug=False):
        self.case = "WireScanner" # other options: 'Screen', 'Grid'
        # common for all cases
        # dw  = 0.03             # [mm] - target thickness, in case of wire-diameter
        # rho = 2e-3             # [g/mm3] - target density
        # wFun= 4.5              # [eV] work function for graphite   2do: temperature-depndent work function!!!
        # wfun=wFun*ttphys.Qe    # [J] ---||----


        self.nparticles = list()  # list of particles crossing the wire in each cell and for each time step (2D)    
        self.npart_prof = list()   # list of number of particles crossing the wire in each step (1D profile)
        self.edeps = list()  # list of ebnergy deposits in the wire cells and for each time step (2D)    

        self.Beam = ttbeam.Beam() # declare beam first!

        print("Target initialization from WireScan constructor:")
        self.Target = ttarget.Target('../material_data/Molybdenum.txt',debug=True)        # standard target
        #self.material_file=Path('../material_data/Molybdenum.txt')
        #print("Target material initialization from WireScan constructor:")
        #self.Target.Material = ttmaterial.Material('../material_data/Molybdenum.txt',debug=True)

        #self.Target.default_init(self.Beam.sigy)                    # initialize standard parameters, no use init in this class
        # set material here
        self.Target.surfMaterial='C'          # sometimes surface material is different than bulk material - not used now
        # simulation parameters
        self.dt = self.Target.depth/self.Target.speed  # it should vary when sublimation becomes important, move to init
        # move definition of start and end positions here?
        # default initialization, move to separate function?
        self.Target.cell_length = self.Beam.sigy/10           # [mm] length of a cell along the wire, move to init
        self.Target.wire_length = 6 * self.Beam.sigy          # [mm] length of the wire, move to init
        # calculate number of cells, it must be odd with central cell covering zero
        self.Target.ncells=int(self.Target.wire_length/self.Target.cell_length) # move to init
        if self.Target.ncells%2==0:
            self.Target.ncells+=1
        if debug:
            print('ncells = ', self.Target.ncells)    
        # how to assure there is zero position?
        self.Target.cell_pos = numpy.arange(-(self.Target.ncells-1)*self.Target.cell_length/2,(self.Target.ncells)*self.Target.cell_length/2,self.Target.cell_length)
        if debug:
            print('cells_pos = ', self.Target.cell_pos, len(self.Target.cell_pos))    
        #self.edepp = # [MeV] energy deposit per particle, using ttarget.Z, shape coeff ,ttbeam.pmass, pcharge and ttphys for Bethe-Bloch calculation
        self.temp_amb = 300 # [K] ambient temperature, needed for radiative cooling
    
    # add Target
    # add beam
    
        
    # move init function from ttarget to here!!!
    def init_default(self):
        '''
        default initialization (vierge wire)
        :return:
        nothing
        '''
        print("default initialization of simulation setup")
        self.Target.depths = [self.Target.depth for i in range(self.Target.ncells)]  # initial radiuses of wire elements (or mean size of pie-shaped screnne elements)
        self.Target.temperature=[[300.0 for i in range(self.Target.ncells)]]     # wire has room temperature at the beginning
        self.Target.thcurr = [ 0.0 for i in range(self.Target.ncells) ]          # thermionic current array
        print("init, ncells = ", self.Target.ncells)
        print("init, temperature = ", self.Target.temperature)
        
        
    def scan(self, debug=False):        
        '''
        Scanning loop

        Returns
        -------
        None.

        '''
        print("run WireScanner starts")
        # scan start and end:
        self.start_pos=-4.0 * self.Beam.sigx  # initial position of the wire
        self.end_pos = 6 * self.Beam.sigx     # final position of the wire
        self.posx = self.start_pos            # initial position
        self.tscan = 0.0                      # simulation start time
        self.part_tot = 0                     # total, accumulated number of particles crossing the wire during scan
        self.part_tot_perstep=0               # 
        self.nparts_along_wire = []           # list of particles hitting the wire at a given step (1D)
                                              # BTW: [] is much (2ms) faster than list()
        print(self.start_pos,self.posx,self.end_pos)          # why they are equal?
        ###################################################
        #   main loop over wire movement
        ###################################################
        while self.posx < self.end_pos:
            print("posx = ",self.posx)
            print( 'time:', self.tscan*1e6, ' [us],  wire pos: ', self.posx, ' [mm],  max temp: ') #, max(ttarget.eTemp), ' [K]'))
            # TODO: move increments to the end of loop
            self.posx += self.Target.speed * self.dt
            print(" POSX  =  ",self.posx)
            self.tscan += self.dt
            self.Target.xPos.append(self.posx)
            self.nparts_along_wire.clear() 
            ###################################################
            # first loop along the wire, over wire cells, to compute heating:
            ###################################################
            self.part_tot_perstep=0
            for cell_idx, cell_centre in enumerate(self.Target.cell_pos):
                # where is timestep here?:
                self.part_cell_step=self.Beam.part_cell(self.posx,cell_centre,self.Target.depth,self.Target.cell_length)*self.dt
                self.nparts_along_wire.append(self.part_cell_step)
                #print(cell_idx,cell_centre,self.part_cell_step)
                self.part_tot_perstep+=self.part_cell_step
            self.npart_prof.append(float(self.part_cell_step))
            self.nparticles.append(self.nparts_along_wire)
            
            self.dtemp_beam = self.beam_heating()
            self.dene_rad   = self.rad_cooling()
            self.dene_therm = self.therm_cooling()
            
            self.a_cp=self.Target.Material.get_Cp(self.Target.temperature[-1])   # specific heat
            self.cell_vol = [math.pi*pow(x,2)*self.Target.cell_length for x in self.Target.depths]     # cell volume        
            self.decool = self.dene_rad + self.dene_therm 
            self.dtcool = [dE/(cp*v*self.Target.Material.rho) for dE,cp,v in zip(self.decool,self.a_cp,self.cell_vol)]
            
            self.Target.temperature.append([t0+dtbeam-dtrad for t0,dtbeam,dtrad in zip(self.Target.temperature[-1],self.dtemp_beam,self.dtcool)])
            
        ###################################################            
        #  at the end of scan
        ###################################################        
        self.part_tot = sum(self.npart_prof)
        #print(self.Target.xPos)
        #print(self.part_tot)
        
        
    def beam_heating(self, debug=False):
        '''
        Beam heating is calculated here.
        '''
        self.temp_along_wire = list()
        # use list comprehension instead of for loop over cells:
        # self.a_cp=self.Target.Material.GetCp([self.Target.temperature[-1]])
        if debug:
            print("beam heating init temp ",self.Target.temperature)
        #self.a_cp=self.Target.Material.get_Cp(max(self.Target.temperature[-1]))   # for testing
        self.a_cp=self.Target.Material.get_Cp(self.Target.temperature[-1])
        self.dtemp1 = [math.pi * self.Beam.dedxef * 1.60218e-13 * 2 * x for x in self.Target.depths]
        self.dtemp2 = [x1 * x2 for x1,x2 in list(zip(self.dtemp1,self.nparts_along_wire))]
        self.cell_vol = [math.pi*pow(x,2)*self.Target.cell_length for x in self.Target.depths]
        self.dtemp_along_wire = [ x1 / (4*x2*cp) for x1,x2,cp in list(zip(self.dtemp2,self.cell_vol,self.a_cp))]
        #print("dtemp along the wire: ",self.dtemp_along_wire, len(self.dtemp_along_wire))
        #print("init temp along the wire: ",self.Target.temperature[-1], len(self.Target.temperature[-1]))
        #print("sum to append: ",[x1+x2 for x1,x2 in list(zip(self.temp_along_wire,self.Target.temperature[-1])]))
        for x1,x2 in zip(self.dtemp_along_wire,self.Target.temperature[-1]):
            print(x1,x2,x1+x2)
        #self.Target.temperature.append([x1+x2 for x1,x2 in zip(self.dtemp_along_wire,self.Target.temperature[-1])]) # no, this should be 2D
        return self.dtemp_along_wire
 
    def rad_cooling(self,debug=False):
        '''
        Here radiative cooling is calculated.
        List coprehension is used to perform the calculation at once along the whole wire.
        
        Parameters
        ----------
        debug : TYPE, optional
            Anount of output, mainly for checking the units. The default is False.
        Returns
        ------
        1D array of amount of heat [Joules] radiated out of the surface of each cell.
        '''
        self.dtemp_list=list()
        self.a_cp=self.Target.Material.get_Cp(self.Target.temperature[-1])
        self.target_esurf = [math.pi*diag*self.Target.cell_length for diag in self.Target.depths]  # radiative surface of cells
        self.cell_vol = [math.pi*pow(x,2)*self.Target.cell_length for x in self.Target.depths]     # cell volume        
        #self.dene = self.target_esurf * constants.Stefan_Boltzmann * self.Target.Material.get_Ems(self.Target.temperature[-1]) * (pow(self.Target.temperature, 4) - pow(self.temp_amb, 4)) * self.dt
        self.dene = [surf*constants.Stefan_Boltzmann*emm*(pow(T,4)- pow(self.temp_amb,4))*self.dt for surf,emm,T in zip(self.target_esurf,self.Target.Material.get_Ems(self.Target.temperature[-1]),self.Target.temperature[-1])] 
        #self.dtemp = [dE/(cp*v*self.Target.Material.rho) for dE,cp,v in zip(self.dene,self.a_cp,self.cell_vol)]
        #self.dtemp = self.dene/(self.a_cp*self.cell_vol*self.Target.Material.rho)
        return self.dene

    def therm_cooling(self,debug=False):
        '''
        Calculation of heat removal due to thermionic cooling.        

        Parameters
        ----------
        debug : TYPE, optional
            Anount of output, mainly for checking the units. The default is False.
        Returns
        -------
        1D array of amount of heat [Joules] removed from each cell.

        '''
        self.target_esurf = [math.pi*diag*self.Target.cell_length for diag in self.Target.depths]  # radiative surface of cells
        self.thCurr = [ttphys.thermionic_current(surf,temp,self.Target.Material.wfun) for surf,temp in zip(self.target_esurf,self.Target.temperature[-1])]
        dene = [2*self.Target.Material.wfun + constants.Boltzmann * temp * thCur * self.dt / constants.e for temp,thCur in zip(self.Target.temperature[-1],self.thCurr)]
        return dene               




       
    def get_prof_npart(self):
        return self.Target.xPos,self.npart_prof
       
        
    def get_temperature(self):
       return self.Target.temperature
       
    def get_max_temperature(self):
        self.maxtemp=[max(x) for x in self.Target.temperature]
        return self.maxtemp   