'''

    The output module (now only for the case of the flying wire.)
    2016.08.23 m.sapinski@gsi.de

    evolution (Evol) - in time
    profile (Prof) - along the wire


'''

import matplotlib.pyplot as plt
# import ttarget, ttsim
from modules import ttbeam, ttsim # needed for scan data saving 
import numpy

eTempProfEvol1D={}  # dictionary where key is time since beginning of simulation 
                    #  and value is a list making temperature profile

eThCurrProfEvol1D={}  # as above but for the thermionic current


# the global simulation figure: -----------------------------------------
fig = plt.figure(0)
# ax = fig.add_subplot(111)
ax1 = fig.add_subplot(211)
ax2 = fig.add_subplot(212)
ax1s = [ax1, ax1.twinx(), ax1.twinx()]          # to make three independent vertical axes
line0, = ax1s[0].plot([0], [0], color='Blue')   # initial plot
line1, = ax1s[1].plot([0], [0], color='Green')  # initial plot
line2, = ax1s[2].plot([0], [0], color='Black')    # initial plot  - beam distribution
ax1s[2].get_yaxis().set_visible(False)
#fig.subplots_adjust(right=0.5) # ugly
ax2.set_autoscale_on(True)
ax2s = [ax2, ax2.twinx()]  # to make two independent vertical axes
line3, = ax2s[0].plot([0], [0], color="Red")  # initial plot
line4, = ax2s[1].plot([0], [0], color='Green') # initial plot


# -----------------------------------------------------------------------

def plotMaxTempEvol(filename='None'):
    '''
     Plots maximum temperature evolution during simulation.
    '''
    print('Evolution of maximum temperature.')
    xx = sorted(eTempProfEvol1D.keys())
    yy = [max(eTempProfEvol1D[x]) for x in xx]
    print(xx)
    print(yy)
    plt.plot(xx,yy)  # why it is not printing
    ax = plt.gca()
    # recompute the ax.dataLim
    ax.relim()
    # update ax.viewLim using the new dataLim
    ax.autoscale_view()
    plt.show()
    if filename!=None:
        plt.savefig(filename)


def plotMaxThCurrEvol(filename='None'):
    '''
    Plots maximum thermionic current evolution during simulation.
    '''
    fig = plt.figure(1)
    ax100 = fig.add_subplot(111)
    print('Evolution of thermionic current.')
    xx = sorted(eThCurrProfEvol1D.keys())
    yy = [ 1e6*max(eThCurrProfEvol1D[ x ]) for x in xx ]
    print(xx)
    print(yy)
    plt.plot(xx, yy)  # why it is not printing
    axa = plt.gca()
    axa.set_xlabel('time [s]', color='Blue')
    axa.set_ylabel('thermionic current [uA]', color='Blue')
    # recompute the ax.dataLim
    axa.relim()
    # update ax.viewLim using the new dataLim
    axa.autoscale_view()
    plt.show()
    if filename != None:
        plt.savefig(filename)


# tested
def saveMaxTempEvol(filename='None'):
    '''
    saves in csv format the maximum temperature evolution plot
    :param filename:
    :return:
    '''
    print('Saving evolution of maximum temperature.')
    xx = sorted(eTempProfEvol1D.keys())
    yy = [ max(eTempProfEvol1D[ x ]) for x in xx ]
    #plt.plot(xx, yy)  # why it is not printing
    #zip(xx, yy)
    if filename == None:
        filename='maxtempevol.csv'
    import csv
    with open(filename, 'w') as f:
        writer = csv.writer(f)
        writer.writerows(list(zip(xx, yy)))


def saveScan(filename='None'):
    '''  
    Parameters
    ----------
    filename : TYPE, optional
        DESCRIPTION. The default is 'None'.

    Returns
    -------
    None.

    Action
    ----
    Saves data file with all data    
    '''
    import csv
    print('ttout saveScan  - saving complete data file')
    xx = sorted(eTempProfEvol1D.keys())
    yy = [ max(eTempProfEvol1D[ x ]) for x in xx ]
    pp = ttsim.wirePositions
    nn = [int(x) for x in ttsim.npartProf]
    ss = ttsim.SEMcurr
    with open(filename, 'w') as f:
        f.write("Simulation parameters:\n")
        f.write("############################ \n")        
        f.write("Title: "+ttsim.title+"\n")  
        f.write("Type: "+ttsim.case+"\n")
        f.write("# beam: \n")
        f.write("ttbeam.sigx = "+str(ttbeam.sigx)+"\n")
        f.write("ttbeam.sigy = "+str(ttbeam.sigy)+"\n")
        f.write("ttbeam.npart [*10^10]= "+str(ttbeam.npart/1e10)+"\n")
        f.write("ttbeam.trev = "+str(ttbeam.trev)+"\n\n")
        f.write("Simulation results:\n")
        f.write("############################ \n")
        f.write("time,maxtemp,position,nparts,SEMcurr\n") # SEMcurrent is in uA
        writer = csv.writer(f)
        writer.writerows(list(zip(xx, yy, pp, nn, ss)))




# test it
def saveMaxThCurrEvol(filename='None'):
    '''
    saves in csv format the maximum thermionic current evolution plot
    :param filename:
    :return:
    '''
    print('Saving evolution of maximum thermionic current.')
    xx = sorted(eThCurrProfEvol1D.keys())
    yy = [ max(eThCurrProfEvol1D[ x ]) for x in xx ]
    # plt.plot(xx, yy)  # why it is not printing
    # zip(xx, yy)
    if filename == None:
        filename = 'maxthcurevol.csv'
    import csv
    with open(filename, 'w') as f:
        writer = csv.writer(f)
        writer.writerows(list(zip(xx, yy)))


# test!
# do also read method later
def saveTempProfile(filename='None'):
    '''
       saves in csv format the (final) temperature profile
    :param filename:
    :return:
    '''
    from modules import ttarget
    print('Saving temperature profile.')
    # plt.plot(xx, yy)  # why it is not printing
    # zip(xx, yy)
    if filename == None:
        filename = 'temp_prof.csv'
    import csv
    with open(filename, 'w') as f:
        writer = csv.writer(f)
        writer.writerows(list(zip(ttarget.xPos, ttarget.eTemp)))

# test!
# do also read method later
def saveRadiiProfile(filename='None'):
    '''
    saves in csv format the (final) array of radii along the wire (after sublimation occures)
    :param filename:
    :return: None
    '''
    from modules import ttarget
    print('Saving radius profile.')
    if filename == None:
        filename = 'radii_prof.csv'
    import csv
    with open(filename, 'w') as f:
        writer = csv.writer(f)
        writer.writerows(list(zip(ttarget.xPos, ttarget.eRad)))


# comment!
def simPlotUpdate(movementTimes, maximumTemps, npartProf):
    from modules import ttarget, ttbeam, ttsim, ttphys
    plt.ion()
    # title can be plotted only once?
    fig.suptitle(ttsim.title, fontsize=12)  # testing
    # set_xdata not needed, because xdata is always the same, except initialization, which is unfortunately outside the
    # function...maybe come back to simPlotInit() function idea - think later
    line0.set_xdata(ttarget.xPos)
    line0.set_ydata(ttarget.eTemp)
    # line 2 could be plotted only once:
    line2.set_xdata(ttarget.xPos)
    # test:
    xBeam = numpy.exp(-0.5*((ttarget.xPos/ttbeam.sigx)**2))
  
    # xBeam=map(lambda x: numpy.exp(-0.5*((x/ttbeam.sigx)**2)), ttarget.xPos)
    #g = max(ttarget.eTemp)*numpy.exp(-0.5*((x/ttbeam.sigx)**2)
    line2.set_ydata(xBeam)
    line1.set_xdata(ttarget.xPos)
    line1.set_ydata(ttarget.eRad)
    ax1s[0].relim()  # recomputes the data limits
    ax1s[2].relim()  # recomputes the data limits
    ax1s[0].autoscale_view(True, True, True)
    ax1s[2].autoscale_view(True, True, True)
    ax1s[0].set_xlabel('position [mm]')
    ax1s[0].grid(True)
    ax1s[0].set_ylim([250, 1.1 * max(ttarget.eTemp)])
    ax1s[0].set_ylabel('temperature [K]', color='Blue')
    ax1s[1].set_ylim([0, 2 * ttarget.depth])
    ax1s[1].set_ylabel('radius [mm]', color='Green')

    #line2, = ax2s[0].plot(movementTimes, maximumTemps)
    #line3, = ax2s[1].plot(movementTimes, npartProf, color='Green')
    #else:
    # it would save computing power,but it does not work
    #    line2.set_xdata(movementTimes)
    #    line2.set_ydata(maximumTemps)
    #    line3.set_xdata(movementTimes)
    #    line3.set_ydata(npartProf)
    line3.set_xdata(movementTimes)
    line3.set_ydata(maximumTemps)
    line4.set_xdata(movementTimes)
    line4.set_ydata(npartProf)
    # print 'ttout:',movementTimes
    ax2s[0].relim()  # recomputes the data limits
    ax2s[0].autoscale_view(True, True, True)
    ax2s[0].set_xlabel('time [s]')
    ax2s[0].set_ylabel('maximum temperature [K]', color='Red')
    ax2s[0].set_ylim([250, 1.1 * max(maximumTemps)])
    ax2s[0].grid(True)
    ax2s[1].set_ylabel('particles on wire', color='Green')
    ax2s[1].set_ylim([0., 1.1 * max(npartProf)])
    # plt.draw()   # problem with replotting
    fig.canvas.draw()
    plt.show()