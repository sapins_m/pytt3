'''

 This module describes target matrial.

 2022.11.29 by mariusz.sapinski@psi.ch, separated from ttarget
 after considerations, a whole class copied from Araceli's code
 adding some comments
     
 2023.02.10 by mariusz.sapinski@psi.ch, added pathlib to use files' pathes 
 independent on operation system
    
 
'''

import sys
import os
import numpy as np
from scipy.interpolate import interp1d

from pathlib import Path


class Material:  # ttmaterial?

    def __init__(self,MaterialFileName,debug=False):
        '''
        Class constructor. Reads file
        Parameters
        ----------
        MaterialFileName : TYPE
            DESCRIPTION.

        Returns
        -------
        None.

        '''
        
        print("Material constructor")
        h = open(MaterialFileName)

        d_MatInfo = {}
        cont = 0
        for l in h:
            if cont == 0:
                cont += 1
                continue

            else:
                if len(l.split()) == 0:
                    print("Error: There are blank spaces in Material Properties file.")
                d_MatInfo.update({l.split()[0] : l.split()[1]})

        self.name = d_MatInfo["Name:"]
        self.mpoint = float(d_MatInfo["MeltingPoint:"])             # [K]
        if debug:
            print("Material initialization: "+self.name)
        self.rho = float(d_MatInfo["Density:"])                     # [g/cm3] Density
        if debug:
            print("Material density: "+str(self.rho))
        self.Z = float(d_MatInfo["Z:"])                             # Atomic Number
        self.Am = float(d_MatInfo["Am:"])                           # Atomic Mass
        self.wfun = float(d_MatInfo["WorkFunction:"])               # [eV] Work function
        if (d_MatInfo["Sublimation_C1:"] != "-" and d_MatInfo["Sublimation_C2:"] != "-"):
            self.Sublimation_C1 = float(d_MatInfo["Sublimation_C1:"])   # Parameter 1 of sublimation formula. 
            self.Sublimation_C2 = float(d_MatInfo["Sublimation_C2:"])    # Parameter 2 of sublimation formula. 
        else: 
            self.Sublimation_C1 = "-"
            self.Sublimation_C2 = "-"

        #  --------- Emissivity --------- #
        EmsInput = d_MatInfo["Emissivity:"]; 
        self.D_Ems = {"Temperature": [], "Parameter": []}
        try: 
            emsT = float(EmsInput)
            self.D_Ems["Temperature"] += [300.0]; self.D_Ems["Parameter"] += [emsT]

        except ValueError:
            if debug:
                print("reading tempemperature dependent emissivity")
            #pathtoems = os.getcwd()+"/../material_data/ParametersWithTemperature/"+EmsInput+".txt"
            EmsInput = str(EmsInput)+".txt"
            pathtoems = Path("../material_data/ParametersWithTemperature") / EmsInput
            
            f_ems = open(pathtoems,"r")
            for j,l in enumerate(f_ems,0):
                if j > 3:
                    self.D_Ems["Temperature"] += [float(l.split()[0])]
                    self.D_Ems["Parameter"] += [float(l.split()[1])]

            f_ems.close()

        self.epsT = self.GetEmissivity(np.array([[300]]))   # Default at 300 K

        # ------------ Specific Heat --------------- #
        CpInput = d_MatInfo["SpecificHeat:"]
        self.D_Cp = {"Temperature": [], "Parameter": []}
        try:
            cpT = float(CpInput)
            self.D_Cp["Temperature"] += [300.0]; self.D_Cp["Parameter"] += [cpT]

        except ValueError:
            #pathtocp = os.getcwd()+"/MaterialInfo/ParametersWithTemperature/"+CpInput+".txt"
            CpInput=str(CpInput)+".txt"
            pathtocp = Path("../material_data/ParametersWithTemperature") / CpInput
            f_cp = open(pathtocp,"r")
            for j,l in enumerate(f_cp,0):
                if j > 3:
                    self.D_Cp["Temperature"] += [float(l.split()[0])]
                    self.D_Cp["Parameter"] += [float(l.split()[1])]
            f_cp.close()

        self.CpT = self.GetCp(np.array([[300]]))            # [J/gK] Specific Heat

        # --------------- Conductivity --------------- #

        Kinput = d_MatInfo["Conductivity:"]
        self.D_k = {"Temperature":[], "Parameter": []}
        try:
            kT = float(Kinput)
            self.D_k["Temperature"] += [300.0]; self.D_k["Parameter"] += [kT]
        
        except ValueError:
            pathtok = os.getcwd()+"../material_data/ParametersWithTemperature/"+Kinput+".txt"
            f_k = open(pathtok,"r")
            for j,l in enumerate(f_k,0):
                if j > 3:
                    self.D_k["Temperature"] += [float(l.split()[0])]
                    self.D_k["Parameter"] += [float(l.split()[1])]
            f_k.close()

        self.con = self.Getk(np.array([[300]]))             # [W/mK] Conductivity

        # ---------------- Sublimation Enthalpy --------------------- #
        Hinput = d_MatInfo["Sublimation_Heat:"]
        
        if (Hinput == "-"):
            Hinput = 0.0 

        self.D_H = {"Temperature":[], "Parameter": []}
        
        try:
            hT = float(Hinput)
            self.D_H["Temperature"] += [300.0]; self.D_H["Parameter"] += [hT]
        
        except ValueError:
            pathtoh = os.getcwd()+"../material_data/ParametersWithTemperature/"+Hinput+".txt"
            f_h = open(pathtoh,"r")
            for j,l in enumerate(f_h,0):
                if j > 3: 
                    self.D_H["Temperature"] += [float(l.split()[0])]
                    self.D_H["Parameter"] += [float(l.split()[1])]
            f_h.close()

        
        
        
        self.HT = self.GetH(np.array([[300]]))              # [J/K] Sublimation enthalpy
        

        self.expcoeff = float(d_MatInfo["ExpansionCoeff:"]) # [1/K] Linear Expansion Coefficient

        h.close()


    def GetParameterValue(self,D_Par,T):
        # All the parameter search follows the same logic.                         #
        # ------------------------------------------------------------------------ #
        # If there is only one value, the parameter will be considered constant.   #
        # Otherwise, we will do a linear extrapolation of the parameter values.    #
        #       par(T<300) = par(T=300)                                            #
        #                                                                          #
        #       par(T) = a*T + b    Where a and b are calculated with the values   #
        #       of the parameter  and temperature that are the upper and lower     #
        #       values of T.                                                       #
        #                                                                          #
        #       par(T) with T > par["Temperature"][-1]. We calculate T as an       #
        #       extrapolation of the last two available values.                    #
        # ------------------------------------------------------------------------ #
        # nice, but use scipy function with eg. quadratic interpolation
        self.Parameter=D_Par["Parameter"][0] # why needed?
        print("debugging GetParameterValue")
        print(D_Par,T)
        if len(D_Par["Temperature"]) == 1:
            self.Parameter = D_Par["Parameter"][0]
        else:
            for k in range(0,len(D_Par["Temperature"])):
          
                if (T <= D_Par["Temperature"][k]) and (k == 0):
                    self.Parameter = D_Par["Parameter"][0]

                    break

                elif (T <= D_Par["Temperature"][k]) and (T <= D_Par["Temperature"][-1]):

                    a = (D_Par["Parameter"][k]-D_Par["Parameter"][k-1])/(D_Par["Temperature"][k]-D_Par["Temperature"][k-1])
                    b = D_Par["Parameter"][k]-a*D_Par["Temperature"][k]
                    self.Parameter = a*T + b
                    
                    break

                elif (T > D_Par["Temperature"][-1] ):
                    print("extrapolation temperature dependence")    
                    a = (D_Par["Parameter"][-1]-D_Par["Parameter"][-2])/(D_Par["Temperature"][-1]-D_Par["Temperature"][-2])
                    b = D_Par["Parameter"][-1]-a*D_Par["Temperature"][-1]
                    self.Parameter = a*T + b

                    break
        print("returning Parameter = "+str(self.Parameter))        
        print("end of GetParameterValue")
        return self.Parameter

    def GetEmissivity(self,Temp,debug=False):
        '''
        Parameters
        ----------
        Temp : is a 2D array, I guess for the need of the screens!
            therefore beware calling it for the wire, it must always be 2D array!
            also be careful, Temp cannot be integers (check!)
        debug :
            DESCRIPTION. The default is False.

        Returns
        -------
        Emissivity : 2D float
            Emissivity array, the same dimension as temperature array.

        '''
        Emissivity = Temp**0  # here emissivity is an array of 1 with the same dimentsion as Temp
        if debug:
            print("debugging GetEmissivity")
            print(Temp)
            #print(Emissivity)
        for i in range(0,len(Emissivity)):
            for j in range(0,len(Emissivity[i])):
                print(self.D_Ems) 
                # self.D_Ems is a dictionary
                #print(Temp[i][j])
                Emissivity[i][j] = self.GetParameterValue(self.D_Ems,Temp[i][j])
                #print(Temp[i][j],Emissivity[i][j])
        return Emissivity


    def GetCp(self,Temp):
        '''
        Parameters
        ----------
        Temp : is a 2D array, I guess for the need of the screens!
            therefore beware calling it for the wire, it must always be 2D array!
        debug :
            DESCRIPTION. The default is False.
        Returns
        -------
        Specific heat (at fixed pressure, should be volume, but for solid it is almost the same) : 2D float
            Specific heat array, the same dimension as temperature array.
        '''
        print("GetCp Temp = ", Temp)
        self.Cp = [[0 for x in Temp]]
        #self.Cp = Temp**0 # this line makes no sense anyway
        print("GetCp Cp = ", self.Cp)
        for i in range(0,len(self.Cp)):
            for j in range(0,len(self.Cp[i])):
                # D_Cp is a dictionary
                print("D_Cp = ",self.D_Cp)
                self.Cp[i][j] = self.GetParameterValue(self.D_Cp,Temp[i][j])
        return self.Cp
    
    
    # sorry Araceli, the above function is a mess, operating on 2D arrays is a mess
    # it must be simplified!
    def get_Cp(self,Temp):
        '''
        Returns specific heat for given temperature.    
        Parameters
        ----------
        Temp : TYPE
            DESCRIPTION.

        Returns
        -------
        TYPE
            DESCRIPTION.

        '''
        self.val=np.interp(Temp,self.D_Cp['Temperature'],self.D_Cp['Parameter'])
        return self.val
    
    def get_Ems(self,Temp):
        '''
        Returns emmisivity for given temperatures.    
        Parameters
        ----------
        Temp : TYPE
            DESCRIPTION.

        Returns
        -------
        TYPE
            DESCRIPTION.

        '''
        self.val=np.interp(Temp,self.D_Ems['Temperature'],self.D_Ems['Parameter'])
        return self.val
    
    

    def Getk(self, Temp):
        '''
        Parameters
        ----------
        Temp : is a 2D array, I guess for the need of the screens!
            therefore beware calling it for the wire, it must always be 2D array!
        debug :
            DESCRIPTION. The default is False.
        Returns
        -------
        Thermal conductivity : 2D float
            Thermal conductivity array, the same dimension as temperature array.

        '''        
        KK = Temp**0
        for i in range(0,len(KK)):
            for j in range(0,len(KK[i])):
                if Temp[i][j] > self.D_k["Temperature"][-1]:
                    KK[i][j] = self.D_k["Parameter"][-1]
                else:
                    KK[i][j] = self.GetParameterValue(self.D_k,Temp[i][j])
        return KK

    def GetH(self, Temp):
        '''
        Parameters
        ----------
        Temp : is a 2D array, I guess for the need of the screens!
            therefore beware calling it for the wire, it must always be 2D array!
        debug :
            DESCRIPTION. The default is False.
        Returns
        -------
        Sublimation enthalpy : 2D float
            Sublimation enthalpy array, the same dimension as temperature array.

        '''                
        HH = Temp**0
        for i in range(0,len(HH)):
            for j in range(0,len(HH[i])):
                HH[i][j] = self.GetParameterValue(self.D_H,Temp[i][j])
        return HH


    # legacy functions, returns specific heat as a single number
    # various parametrization are introduced by hand
    def getSHeat(self,material="C", temp=300.0):
        '''
        Parameters
        ----------
        material : string, optional
            Material type. The default is "C" for Carbon.
            temp : float, optional
            Temperature of material [K]. The default is 300.
            Returns
            -------
            float
            Specific heat of the material at given temperature.
        '''
        if material=="C":
            return self.getSHeat_C(temp)
        elif material=="W":
            return self.getSHeat_W(temp)
        elif material=="Mo":
            return self.getSHeat_Mo(temp)
        else:
            print( "target material not specified")

    # legacy
    def getSHeat_C(self,temp=300):
        '''
        Function describing the specific heat of the material (graphite) as a function
        of the temperature. Default value of the temperature is 273 K.
        Function from original WireTemp2D.C. Return specific heat in J/gK.
        '''
        #print("ttarget::getSHeat_C")
        self.a1 = 2.97
        self.a2 = -35.1
        self.a3 = 0.49
        self.b1 = -0.012
        self.b2 = 0.0026

        self.cf = self.a1 + (self.a2 / pow(temp, self.a3))
        self.cfl = self.b2 * temp + self.b1

        self.ext_fact = 1.0
        if temp < 5:
            return self.ext_fact * 1e-3
        elif temp >= 5 and temp < 250.:
            return self.ext_fact * self.cfl
        elif temp >= 250:
            return self.ext_fact * self.cf

    # legacy
    def getSHeat_W(self,temp=300):
        '''
        Function describing the specific heat of the material (tungsten) as a function
        of the temperature. Default value of the temperature is 300 K.
        Returns specific heat in J/gK.
        Use 1d interpolation on some data (in the future get better data!)
        '''
        self.temper=[298.0, 300.0, 1500.0, 2000.0, 2500.0, 3000.0, 3500.0, 3700.0]
        self.Sheat=[0.134, 0.134, 0.147, 0.169, 0.180, 0.196, 0.283, 0.337]
        self.finter = interp1d(self.temper, self.Sheat, kind='cubic')
        # print 'asked for temp = ', temp
        return self.finter(temp)

    # legacy
    def getSHeat_Mo(self,temp=300):
        '''
        Function describing the specific heat of the material (Molybdenum) as a function
        of the temperature. Default value of the temperature is 300 K.
        Returns specific heat in J/gK.
        Use 1d interpolation on some data (in the future get better data!)
        '''
        self.temper=[300.0, 400.0, 600.0, 800.0, 900.0, 1100.0, 1400.0, 1800.0, 2000.0, 2400.0, 2600.0, 2800.0]
        self.Sheat=[0.2197, 0.2364, 0.2636, 0.2741, 0.2812, 0.2929, 0.3096, 0.3410, 0.3824, 0.4184, 0.4310, 0.5648]
        if temp<max(self.temper) and temp>min(self.temper):
            self.finter = interp1d(self.temper, self.Sheat, kind='cubic')
            self.fout=self.finter(temp)
        elif temp>=max(self.temper):
            self.fout = max(self.Sheat)
        else:
            self.fout = min(self.Sheat)
        return self.fout

    # legacy
    def getThConductivity(self,material="C",temp=300):
        '''
        :Parameters - material and temperature in Kelvin [K]: valid from 300K to sublimation temperature.
        '''
        if material=="C":
            return self.getThConductivity_C(temp)
        elif material=="W":
            return 2*self.getThConductivity_C(temp)  # preliminary/wrong
        else:
            print("unknown material!")

    # legacy
    # to do: values below 300K (exist in WireTemp2D.C)
    def getThConductivity_C(self,temp=300):
        '''
        :Parameter - temperature in Kelvin [K]: valid from 300K to sublimation temperature.
        :Returns - graphite thermal conductivity in [W/(mm*K)]:
        '''
        self.p0=1.19899
        self.p1=-9.06797e-04
        self.p2= 3.2114e-07
        self.p3=-3.93710e-11
        self.val1=self.p0+(self.p1*temp)+(self.p2*temp**2)+(self.p3*pow(temp,3))
        self.val1=self.val1/10   # needed in order to have mm in units
        if temp<359:
            return 0.0913
        else:
            return self.val1












