# -*- coding: utf-8 -*-
"""
Created on Fri Feb 24 13:51:54 2023

@author: sapins_m

Class SemGrid, controls running SEM grid simulation

Here we iterate over beam pulse, not over wire movement as in WireScanner class.

"""

import numpy, math
from modules import ttbeam, ttphys, ttmaterial, ttarget
from scipy.interpolate import interp1d
from pathlib import Path
from scipy import constants


class SemGrid:
    
    def __init__(self,debug=False):
        self.case = "SemGrid"