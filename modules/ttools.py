'''

 some tools, is this really needed?

'''


# returns 2-tuple: array element closes to the value and its index
# currently not used (2016.08.19)? - remove?
def find_nearest(array,value):
    idx = numpy.searchsorted(array, value, side="left")
    if idx > 0 and (idx == len(array) or math.fabs(value - array[idx-1]) < math.fabs(value - array[idx])):
        return array[idx-1], idx-1
    else:
        return array[idx], idx

# does it really fit here:?
def readVapourPressureData(filename="data/vappress.data"):
    '''
    reading vapour pressure data
    :param filename: name of the data file
    :return: list of temperatures and vapour pressures
    '''
    outt=[]
    outv=[]
    file=open(filename,mode='r')
    for line in file:
        ldata=line.split()
        outt.append(float(ldata[0]))
        outv.append(float(ldata[1]))
    file.close()
    return outt, outv

