# -*- coding: utf-8 -*-

'''

    This is a main simulation file for the case of the flying wire.
    It defines the type of simulation.
    To run: > python simXXX.py
    2022.08.09 mariusz.sapinski@psi.de

'''

from modules import ttbeam, ttarget, ttsim, ttout, ttphys
import math
import numpy
import matplotlib.pyplot as plt

##########################################################
#               define simulation type/title             #
##########################################################
ttsim.case = "WireScanner"
ttsim.title="PSI profile monitor"  # use for printout


##########################################################
#                   define the beam                      #
#           pKanal 590 beam pulse to UCN                 #
##########################################################
# p_kanal beams at location of test monitor MBPT12:
ttbeam.sigx = 6.2         # [
ttbeam.sigy = 1.3
ttbeam.npart = 1.12e16  # total intensity of the beam/s
ttbeam.trev = 1.0         # [s]

##########################################################
#               define the target/detector               #
#                 7 um carbon wire                       #
##########################################################
# stopping power of 3 GeV protons in carbon:
ttarget.dEdx=1.755e2    # [MeV*mm2/g], as used in the paper <<<
ttarget.depth = 0.033    # [mm] wire thickness
ttarget.vw = 60          # [mm/s] wire speed
ttarget.dh = 0.033       # [mm]

# target is 33 um thick Molybdenum wire
#ttarget.material="Mo"  # Molybdenum
ttarget.material="C"  # Molybdenum
ttarget.density = 10.3e-3  # [g/mm3]

# 590 MeV protons in Molybdenum, from NIST p-star database:
ttarget.dEdx = 1.642e2 # MeV*mm2/g 
#ttarget.dEdx= 22000./(ttarget.density*ttarget.depth)  # [MeV*mm2/g] value from paper, a bit of reverse engineering
                                                        # is equal to 128.5 [MeV*mm2/g], a bit too small?
                                                        # maybe correct because it takes into account kick-off electrons

ttarget.molarMass = 95.95 # [g/mol]
ttarget.awFun = 4.4                       # [eV], wikipedia says between 4.36 and 4.95
ttarget.wfun = ttarget.awFun * ttphys.Qe  # work function in Joules, as needed
# from http://www.engineeringtoolbox.com/emissivity-coefficients-d_447.html
ttarget.emissivity=0.2   # good for about 1500 C, it increses with temperature

ttarget.init()  # it is obligatory to run this!!! (do: incorporate in runWireScanner()?)
print("slevel: ttarget initialized")

##########################################################
#       define the rest of the simulation parameters     #
##########################################################
# overwrite the end of the scan position:
ttsim.startScan= ttbeam.sigy
ttsim.endScan= ttbeam.sigy
ttsim.dt=5e-4

# overwrite which cooling processes should be executed:
ttsim.radiativeCooling=True
ttsim.conductiveCooling=False
ttsim.thermionicCooling=True
ttsim.sublimation=True

print("running the simulation:")
ttsim.runWireScanner()

ttout.plotMaxTempEvol('out1.png')

ttout.saveTempProfile('MBPT12_5sigma.csv')

ttout.saveMaxTempEvol('MBPT12_tempEvol.csv')


#print(max(ttarget.eAccNparts))
# print ttarget.eAccTemp
print(len(ttarget.xPos), len(ttarget.eAccNparts), len(ttsim.nparts))
# print len(ttarget.xPos),len(ttarget.eAccTemp)
# print wirePositions
# plt.plot(ttarget.xPos,ttarget.eAccTemp)
# plt.show()
