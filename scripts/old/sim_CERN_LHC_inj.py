# -*- coding: utf-8 -*-

'''

    This is a main simulation file for the case of the flying wire.
    It defines the type of simulation.
    To run: > python sim.py
    2016.08.16 m.sapinski@gsi.de

'''

from modules import ttbeam, ttarget, ttsim, ttout, ttphys
import math
import numpy
import matplotlib.pyplot as plt

##########################################################
#               define simulation type/title             #
##########################################################
ttsim.case = "WireScanner"
ttsim.title="CERN LHC Flying Wire"  # use for printout


##########################################################
#                   define the beam                      #
#           LHC 450 GeV proton  beam at injection        #
##########################################################
# LHC at injection case from the main paper
ttbeam.sigx = 0.8
ttbeam.sigy = 0.53
ttbeam.npart = 0.25*3.2e14  # total intensity of the circulating beam
ttbeam.trev = 89e-6         # [s]

##########################################################
#               define the target/detector               #
#                 7 um carbon wire                       #
##########################################################
# stopping power of 3 GeV protons in carbon:
#ttarget.dEdx=1.755e2    # [MeV*mm2/g], as used in the paper <<<
ttarget.depth = 0.033    # [mm] wire thickness
ttarget.vw = 1000       # [mm/s] wire speed
ttarget.dh = 0.033         # [mm]

# target is 33 um thick carbon fiber
ttarget.material="C"  # carbon
ttarget.density = 2.0e-3  # [g/mm3]

ttarget.dEdx= 22000./(ttarget.density*ttarget.depth)  # [MeV*mm2/g] value from paper, a bit of reverse engineering
                                                        # is equal to 128.5 [MeV*mm2/g], a bit too small?
                                                        # maybe correct because it takes into account kick-off electrons

ttarget.molarMass = 12.01
ttarget.awFun = 4.0                       # [eV],
ttarget.wfun = ttarget.awFun * ttphys.Qe  # work function in Joules, as needed
# from http://www.engineeringtoolbox.com/emissivity-coefficients-d_447.html
ttarget.emissivity=1.0   #

ttarget.init()  # it is obligatory to run this!!! (incorporate in runWireScanner()?)

##########################################################
#       define the rest of the simulation parameters     #
##########################################################
# overwrite the end of the scan position:
ttsim.startScan= ttbeam.sigy
ttsim.endScan= ttbeam.sigy
ttsim.dt=1e-6

# overwrite which cooling processes should be executed:
ttsim.radiativeCooling=True
ttsim.conductiveCooling=True
ttsim.thermionicCooling=True
ttsim.sublimation=True

print("running the simulation:")
ttsim.runWireScanner()

#ttout.plotMaxTempEvol('out1.png')

ttout.saveTempProfile('LHC_5sigma.csv')

ttout.saveMaxTempEvol('LHC_tempEvol.csv')


#print(max(ttarget.eAccNparts))
# print ttarget.eAccTemp
print(len(ttarget.xPos), len(ttarget.eAccNparts), len(ttsim.nparts))
# print len(ttarget.xPos),len(ttarget.eAccTemp)
# print wirePositions
# plt.plot(ttarget.xPos,ttarget.eAccTemp)
# plt.show()
