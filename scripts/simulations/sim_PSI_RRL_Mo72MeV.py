# -*- coding: utf-8 -*-

'''

    This is a main simulation file for the case of the flying wire.
    It defines the type of simulation.
    To run: > python simXXX.py
    2022.08.09 mariusz.sapinski@psi.de
    2023.02.24 - corrected to be used with new, object-oriented pytt3

'''



import sys
sys.path.append("..") # Adds higher directory to python modules path.

from pathlib import Path
from modules import ttwirescan, ttmaterial
import matplotlib.pyplot as plt

##########################################################
#               define simulation type/title             #
##########################################################
RRLscan = ttwirescan.WireScan(debug=True)
RRLscan.init_default()

# here should be a function printing all settings 

#ttsim.case = "WireScanner"
#ttsim.title="PSI Main Ring RRL"  # use for printout


##########################################################
#                   define the beam                      #
#                72 MeV 2 mA beam                        #
##########################################################
# p_kanal beams at location of test monitor MBPT12:
RRLscan.Beam.sigx = 1.25         # [mm]
RRLscan.Beam.sigy = 1.0          # [mm]
RRLscan.Beam.ene = 72            # [MeV]
RRLscan.Beam.dedxef=836.8*3.14/4*0.5 # effective dE/dx, takes into account escaping electrons (0.5) and cyllindrical shape (pi/4)         
RRLscan.Beam.paps=1.25e+16       # [particles per second] - beam current, correspond to 2 mA

##########################################################
#               define the target/detector               #
#                 33 um C wire                          #
##########################################################
RRLscan.Target.Material = ttmaterial.Material("../material_data/Carbon.txt")
RRLscan.Target.depth = 0.033        # [mm] wire diameter
RRLscan.Target.speed = 29.7           # [mm/s] wire speed

RRLscan.scan()

# plot output
plt.plot(RRLscan.get_prof_npart()[0], RRLscan.get_max_temperature()[:-1],'o')
plt.xlabel("wire position [mm")
plt.ylabel("maximum temperature [K]")
plt.show()

