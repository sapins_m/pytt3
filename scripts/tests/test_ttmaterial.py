# -*- coding: utf-8 -*-
"""
Created on Fri Feb 10 16:00:38 2023

testing ttmaterial class
runs on windows

@author: sapins_m
"""




import sys
sys.path.append("..") # Adds higher directory to python modules path.

from pathlib import Path
from modules import ttmaterial
import matplotlib.pyplot as plt

material_file=Path('../material_data/Molybdenum.txt')

Material = ttmaterial.Material(material_file)

#print(Material.GetCp(800))


print("material Cp: ")
print(Material.D_Cp)

plt.plot(Material.D_Cp["Temperature"],Material.D_Cp["Parameter"],'-o')
plt.xlabel("Temperature [K]")
plt.ylabel("Cp [J/gK]")
plt.show()

