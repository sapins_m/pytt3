# -*- coding: utf-8 -*-
"""
Created on Tue Feb 21 10:46:57 2023

@author: sapins_m
"""

import sys
sys.path.append("../..") # Adds higher directory to python modules path.

from pathlib import Path
from modules import ttwirescan
import matplotlib.pyplot as plt

#material_file=Path('../material_data/Molybdenum.txt')

print('calling WireScan constructor:')
WireScan = ttwirescan.WireScan(debug=True)
#Material = ttmaterial.Material(material_file)
print('init')
WireScan.init_default()
WireScan.scan()

plt.plot(*WireScan.get_prof_npart(),'o-')
plt.show()

print(WireScan.get_temperature())
plt.plot(WireScan.get_prof_npart()[0], WireScan.get_max_temperature()[:-1],'o')
plt.show()

# 2D print
#ax = fig.add_subplot(111)
plt.imshow(WireScan.get_temperature())
plt.show()
#ax.set_aspect('equal')


