
========================================================================================================================
Thin Target simulation code
version: 2016.10.12
author: m.sapinski@gsi.de

__Description:__
This program estimates temperature of a thin target hit by a particle beam.
Used units: cm->mm(check), s, K, g, J
Three cases:
1. WireScanner:
   the wire moves in x direction and is placed along y-direction
2. SEM grid
3. scintillating screen (to be done)

__Scope:__
Program takes into account: target heating, heat capacity, conductive cooling (watch out, sometimes gets instable),
radiative cooling and thermionic cooling. It estimates thermionic current and sublimation of the material.

__How to Run:__
look at any of the sim*.py files. They contain various simulation cases. To run,simply do:
> python -i sim_JPARC_MR_30GeV.py
Program was developed and tested on Linux, using python 2.7.10 with GCC 5.3.1 20160406 (Red Hat 5.3.1-6)
matplotlib.__version__ = '1.5.3'
scipy.__version__ = '0.18.1'


__Limits:__
1. only gaussian beams for the moment

========================================================================================================================


Developers remarks:

  Conventions for variable naming: (*)
P-physics
T-target  (ttarget module)
B-beam    (ttbeam module)
C-cell - target element (bin)  ttcell???
(*) 2016.08.20 - convention is not used, maybe to be applied in the future,
    maybe not needed, as we have modules now (ttphys module?)

ttmat - material properties?
ttout - output (showTempProfileCenter...)

coding conventions from here (mainly):
http://python.net/~goodger/projects/pycon/2007/idiomatic/handout.html
and using PEP-8 rules build-in pycharm.

t-time
temp-temperature
part-particles

gitlab:
git remote add origin https://gitlab.com/Sapinski/pyTT.git
git add .
[sapinski@zena pyTT]$ git commit
[master 72b1b78] commit 2
 5 files changed, 450 insertions(+), 1 deletion(-)
 create mode 100644 .idea/misc.xml
 create mode 100644 .idea/modules.xml
 create mode 100644 .idea/pyTT.iml
 create mode 100644 .idea/workspace.xml
[sapinski@zena pyTT]$ git push -u origin master
Counting objects: 27, done.
Delta compression using up to 4 threads.
Compressing objects: 100% (26/26), done.
Writing objects: 100% (27/27), 18.08 KiB | 0 bytes/s, done.
Total 27 (delta 2), reused 0 (delta 0)
To https://gitlab.com/Sapinski/pyTT.git
 * [new branch]      master -> master
Branch master set up to track remote branch master from origin.

added via VCS meanu in pycharm

pycharm - open project: File/Open Recent  - works (2016.09.28)

ToDo:
1. subtract temperature drop from all cooling methods at once!


=======================================================
